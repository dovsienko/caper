#!/bin/bash
# Caper: a pcap expression analysis utility.
# Nik Sultana, February 2023
#
# Run all regression tests and summarise results.
#
# Example command to update expected results for a test:
#   ./run_syntax_tests.sh > tests/syntax_test.results
#   ./run_disambiguate_tests.sh > tests/disambiguate_test.results
#   ./run_pp_tests.sh > tests/pp_regression.results
#   ./run_idempotence_tests.sh > tests/idempotence.results
#   ./run_notundistribute_tests.sh > tests/notundistribute.results

# FIXME sensitive to the directory in which this is run

MODE=$1

function run_test() {
  TEST_NAME="$1"
  TEST_SCRIPT="$2"
  TEST_EXPECTED_RESULTS="$3"
  if [ "${MODE}" == "dontcompare" ]
  then
    eval ${TEST_SCRIPT}
  else
    echo "Running ${TEST_NAME}..."
    diff --suppress-common-lines -y <(${TEST_SCRIPT}) <(cat ${TEST_EXPECTED_RESULTS})
    if [ "$?" -eq 0 ]
    then
      echo -n "${TEST_NAME} "
      tput setaf 2
      tput smso
      echo "PASSED"
      tput rmso
      tput sgr0
    else
      echo -n "${TEST_NAME} "
      tput setaf 1
      tput smso
      echo "FAILED"
      tput rmso
      tput sgr0
    fi
  fi
}

time run_test "Syntax Test" "./run_syntax_tests.sh" "tests/syntax_test.results"
echo
time run_test "Disambiguate Test" "./run_disambiguate_tests.sh" "tests/disambiguate_test.results"
echo
time run_test "Pretty-printer Test" "./run_pp_tests.sh" "tests/pp_regression.results"
echo
time run_test "Idempotence Test" "./run_idempotence_tests.sh" "tests/idempotence.results"
echo
time run_test "Weak simplification Test ('-not_undistribute')" "./run_notundistribute_tests.sh" "tests/notundistribute.results" # FIXME work-in-progress
echo

if [ -n "${CAPER_WITH_ENGLISH}" ]
then
  time run_test "English to Pcap Test" "./run_english_to_pcap_tests.sh" "tests/english_to_pcap_regression.results"
  echo
  time run_test "Pcap to English Test" "./run_pcap_to_english_tests.sh" "tests/pcap_to_english_regression.results"
  echo
fi

time run_test "BPF regression Test" "./run_bpf_tests.sh" "tests/bpf_regression_test.results"
echo
time run_test "Optimized BPF regressions Test" "./run_optimized_bpf_tests.sh" "tests/optimized_bpf_regression_test.results"
echo
#time run_test "Exit Status Test" "./run_exitstatus_tests.sh" "tests/exitstatus.results" # FIXME work-in-progress
#echo
# FIXME add tests for HTML output and pretty-printed HTML output.
