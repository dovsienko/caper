(*
  Copyright Hyunsuk Bang, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: auxiliary functions
*)

open Inst
open Headers

(* tail_recursive List.rev *)
let lst_rev lst =
  let rec rev_tail_rec acc lst =
    match lst with
    | [] -> acc
    | hd :: tl -> rev_tail_rec (hd :: acc) tl
  in
  rev_tail_rec [] lst

(* tail_recursive lst_map *)
let lst_map f lst =
  let rec lst_map_helper f lst acc =
    match lst with
    | [] -> lst_rev acc
    | x :: xs -> lst_map_helper f xs (f x :: acc)
  in
  lst_map_helper f lst []

let assoc_lst_map f lst =
  let rec lst_map_helper f lst acc =
    match lst with
    | [] -> lst_rev acc
    | (a, b) :: xs -> lst_map_helper f xs ((a, f b) :: acc)
  in
  lst_map_helper f lst []

(* tail recursive List.append *)
let lst_append lst1 lst2 =
  let rec lst_append_helper acc l1 l2 =
    match l1 with
    | [] -> List.rev_append acc l2
    | hd :: tl -> lst_append_helper (hd :: acc) tl l2
  in
  lst_append_helper [] lst1 lst2

let header_size header =
  match header with
  | "ether" -> 14
  | "ip" -> 20
  | "ip6" -> 40
  | _ -> failwith "unexpected header"

let conjoin (sf1 : sock_filter option) (sf2 : sock_filter option) : sock_filter option =
  match sf1, sf2 with
  | Some {code = [Nil_op]; cond = No_cond; jt = None; jf = None;}, sf
  | sf,  Some {code = [Nil_op]; cond = No_cond; jt = None; jf = None;} -> sf
  | _, _ -> Some {code = [Logical_And]; cond = True_cond; jf = sf2; jt = sf1}

let disjoin (sf1 : sock_filter option) (sf2 : sock_filter option) : sock_filter option =
  match sf1, sf2 with
  | Some {code = [Nil_op]; cond = No_cond; jt = None; jf = None;}, sf
  | sf,  Some {code = [Nil_op]; cond = No_cond; jt = None; jf = None;} -> sf
  | _, _ -> Some {code = [Logical_Or]; cond = True_cond; jf = sf2; jt = sf1;}

let rec negate (sf : sock_filter option) : sock_filter option =
  match sf with
  | Some {code = [Nil_op]; cond = No_cond; jt = None; jf = None;} -> Some {code = [Nil_op]; cond = No_cond; jt = None; jf = None;}
  | Some {code = [Logical_And]; cond = True_cond; jf = sf2; jt = sf1} -> Some {code = [Logical_Or]; cond = True_cond; jf = negate sf2; jt = negate sf1;}
  | Some {code = [Logical_Or]; cond = True_cond; jf = sf2; jt = sf1} -> Some {code = [Logical_And]; cond = True_cond; jf = negate sf2; jt = negate sf1;}
  | Some {code = x; cond = y; jf = ret_false; jt = ret_true} -> Some {code = x; cond = y; jf = ret_true; jt = ret_false}
  | Some {code = x; cond = y; jf = ret_true; jt = ret_false} -> Some {code = x; cond = y; jf = ret_false; jt = ret_true}

let conjoin_from_stack (stack : sock_filter option list) : sock_filter option list =
  let sf2 = List.hd stack in
  let sf1 = List.hd (List.tl stack) in
  (conjoin sf1 sf2) :: (List.tl (List.tl stack))

let disjoin_from_stack (stack : sock_filter option list) : sock_filter option list =
  let sf2 = List.hd stack in
  let sf1 = List.hd (List.tl stack) in
  (disjoin sf1 sf2) :: (List.tl (List.tl stack))

let negate_from_stack (stack : sock_filter option list) : sock_filter option list =
  let sf = List.hd stack in
  (negate sf) :: (List.tl stack)

let is_digit (digit : string) : bool =
  Str.string_match (Str.regexp "[0-9]+$") digit 0

let is_offset (proto : string) : bool =
  Str.string_match (Str.regexp ".*\\[.*\\]") proto 0

let is_expr_size (offset : string) : bool =
  if Str.string_match (Str.regexp ".*:.*") offset 0 then
    let x = String.split_on_char ':' offset
            |> List.map String.trim
            |> List.rev
            |> List.hd in
    String.length x = 1
  else false

let is_array (pcap : string) : bool =
  is_expr_size pcap || is_offset pcap

let is_hex (hex : string) : bool =
  Str.string_match (Str.regexp "0x[0-9a-fA-F]+$") hex 0

let is_arith_op (op : string) : bool =
  Str.string_match (Str.regexp "\\(\\+\\|-\\|\\*\\|\\/\\|%\\||\\|&\\|<<\\|>>\\)$") op 0

let src_ip_offset = function
  | "ip" -> 12
  | "arp" -> 14
  | "rarp" -> 14
  | "ip6" -> 8

let dst_ip_offset = function
  | "ip" -> 16
  | "arp" -> 24
  | "rarp" -> 24
  | "ip6" -> 24

let ipv4_mask (net : string list) : int =
  match net with
  | [oct1] -> 0xff000000
  | [oct1; oct2] -> 0xffff0000
  | [oct1; oct2; oct3;] -> 0xffffff00
  | [oct1; oct2; oct3; oct4] -> 0xffffffff

let ipv4_to_lit (net : string list) : int =
  match net with
  | [oct1] ->
    Int.shift_left (int_of_string oct1) 24
  | [oct1; oct2] ->
    Int.shift_left (int_of_string oct1) 24
    + Int.shift_left (int_of_string oct2) 16
  | [oct1; oct2; oct3;] ->
    Int.shift_left (int_of_string oct1) 24
    + Int.shift_left (int_of_string oct2) 16
    + Int.shift_left (int_of_string oct3) 8
  | [oct1; oct2; oct3; oct4] ->
    Int.shift_left (int_of_string oct1) 24
    + Int.shift_left (int_of_string oct2) 16
    + Int.shift_left (int_of_string oct3) 8
    + (int_of_string oct4)
  | _ -> failwith "Util.ipv4_to_lit"

let ipv6_mask (mask : int) =
  let rec ipv6_mask_helper (shift : int) (mask : int)=
    if (mask mod 4) <> 0 then
      failwith "util.mask_ipv6"
    else if mask = 0 then 0
    else (Int.shift_left 0xf (4 * shift)) + ipv6_mask_helper (shift - 1) (mask - 4)
  in
  ipv6_mask_helper 7 mask

let rec ipv6_pad (ipv6_addr : string list) =
  if List.length ipv6_addr < 8 then ipv6_pad (ipv6_addr @ ["0000"])
  else ipv6_addr

let rec slice_ipv6 (ipv6_addr : string list) (idx : int) =
  match ipv6_addr with
  | [x0;x1;x2;x3;x4;x5;x6;x7] ->
    if idx = 0 then [x0; x1]
    else if idx = 1 then [x2; x3]
    else if idx = 2 then [x4; x5]
    else if idx = 3 then [x6; x7]
    else failwith "util.slice_ipv6"
  | _ -> slice_ipv6 (ipv6_pad ipv6_addr) idx

let ipv6_piece_to_lit (net : string list) (idx : int) : int =
  let helper (ipv6_piece : string list) =
    match ipv6_piece with
    | [x; y] -> Int.shift_left (int_of_string ("0x" ^ x)) 16 + int_of_string ("0x" ^ y) in
  helper (slice_ipv6 net idx)

let rec make_recur ceil sf =
  if ceil = 1 then
    sf
  else
   disjoin sf (make_recur (ceil - 1) sf)

let abort_bpf_gen (err_msg : string) =
  Printf.eprintf "BPF compilation error: %s\n" err_msg;
  begin
    if !Config.parse_and_prettyprint_mode then
      Printf.printf "(000) ret #0\n"
    else
      Printf.printf "l000: ret #0\n"
  end;
  exit 1;