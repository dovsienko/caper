(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: ip protocol
*)

open Inst
open Util
open Host
open Net
open Headers
open Protochain

let ipv4_type = function
  | "ip" -> 0x0
  | "icmp" -> 0x1
  | "tcp" -> 0x6
  | "udp" -> 0x11
  | "sctp" -> 0x84
  | _ -> failwith "Ip.ipv4_type"

let ipv4_frag (headers : packet_headers) x : sock_filter option =
  Some {code = [Ldh (Off (sum_of_header headers x + 6))]; cond = Jset (Hexj 0x1fff); jt = ret_false; jf = ret_true;}

let ip_ihl (headers : packet_headers) : opcode =
  match headers with
  | Packet (x, _, L3 Ip) | Segment (x, _, L3 Ip, _) -> Ldxb (Exp ((Lit 4), (Exp ((Off (sum_of_header headers "ip")), (Hex 0xf), Arith_and)), Multiplication))

let ip_proto (protocol : string) (headers : packet_headers) : sock_filter option =
  let sum_of_pred_headers = sum_of_header headers "ip" in
  Some {
    code = [
      Ldb (Off (sum_of_pred_headers + 9))
    ];
    cond = Jeq (Hexj (ipv4_type protocol));
    jt = ret_true;
    jf = ret_false;
  }

let ip_to_sock_filter (ip_info : string list) (headers : packet_headers) : sock_filter option =
  match ip_info with
  | "proto" :: [protocol] -> ip_proto protocol headers
  | "src" :: "and" :: "dst" :: "host" :: [host] -> conjoin (src_host "ip" host headers) (dst_host "ip" host headers)
  | "src" :: "or" :: "dst" :: "host" :: [host] -> disjoin (src_host "ip" host headers) (dst_host "ip" host headers)
  | "src" :: "host" :: [host] -> src_host "ip" host headers
  | "dst" :: "host" :: [host] -> dst_host "ip" host headers
  | "src" :: "and" :: "dst" :: "net" :: [net] -> conjoin (src_net "ip" net headers) (dst_net "ip" net headers)
  | "src" :: "or" :: "dst" :: "net" :: [net] -> disjoin (src_net "ip" net headers) (dst_net "ip" net headers)
  | "src" :: "net" :: [net] -> src_net "ip" net headers
  | "dst" :: "net" :: [net] -> dst_net "ip" net headers
  | "protochain" :: [protochain_num] ->
    Env.generate_protochain := true;
    Env.protochain_list := (int_of_string protochain_num :: !Env.protochain_list);
    protochain "ip" protochain_num headers
  | _ -> abort_bpf_gen "Ip.ip_to_sock_filter"
