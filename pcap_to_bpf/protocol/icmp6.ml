(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: icmp6 protocol
*)

open Inst
open Headers
open Protochain
open Util

let icmp6_protochain_asts = ref []

let is_icmp6_type = function
| "icmp6-destinationunreach"
| "icmp6-packettoobig"
| "icmp6-timeexceeded"
| "icmp6-parameterproblem"
| "icmp6-echo"
| "icmp6-echoreply"
| "icmp6-multicastlistenerquery"
| "icmp6-multicastlistenerreportv1"
| "icmp6-multicastlistenerdone"
| "icmp6-routersolicit"
| "icmp6-routeradvert"
| "icmp6-neighborsolicit"
| "icmp6-neighboradvert"
| "icmp6-redirect"
| "icmp6-routerrenum"
| "icmp6-nodeinformationquery"
| "icmp6-nodeinformationresponse"
| "icmp6-ineighbordiscoverysolicit"
| "icmp6-ineighbordiscoveryadvert"
| "icmp6-multicastlistenerreportv2"
| "icmp6-homeagentdiscoveryrequest"
| "icmp6-homeagentdiscoveryreply"
| "icmp6-mobileprefixsolicit"
| "icmp6-mobileprefixadvert"
| "icmp6-certpathsolicit"
| "icmp6-certpathadvert"
| "icmp6-multicastrouteradvert"
| "icmp6-multicastroutersolicit"
| "icmp6-multicastrouterterm" -> true
| _ -> false

let icmp6_type_to_opcode = function
| "icmp6-destinationunreach" -> Num 1
| "icmp6-packettoobig" -> Num 2
| "icmp6-timeexceeded" -> Num 3
| "icmp6-parameterproblem" -> Num 4
| "icmp6-echo" -> Num 128
| "icmp6-echoreply" -> Num 129
| "icmp6-multicastlistenerquery" -> Num 130
| "icmp6-multicastlistenerreportv1" -> Num 131
| "icmp6-multicastlistenerdone" -> Num 132
| "icmp6-routersolicit" -> Num 133
| "icmp6-routeradvert" -> Num 134
| "icmp6-neighborsolicit" -> Num 135
| "icmp6-neighboradvert" -> Num 136
| "icmp6-redirect" -> Num 137
| "icmp6-routerrenum" -> Num 138
| "icmp6-nodeinformationquery" -> Num 139
| "icmp6-nodeinformationresponse" -> Num 140
| "icmp6-ineighbordiscoverysolicit" -> Num 141
| "icmp6-ineighbordiscoveryadvert" -> Num 142
| "icmp6-multicastlistenerreportv2" -> Num 143
| "icmp6-homeagentdiscoveryrequest" -> Num 144
| "icmp6-homeagentdiscoveryreply" -> Num 145
| "icmp6-mobileprefixsolicit" -> Num 146
| "icmp6-mobileprefixadvert" -> Num 147
| "icmp6-certpathsolicit" -> Num 148
| "icmp6-certpathadvert" -> Num 149
| "icmp6-multicastrouteradvert" -> Num 151
| "icmp6-multicastroutersolicit" -> Num 152
| "icmp6-multicastrouterterm" -> Num 153
| _ -> failwith "Icmp6.icmp6_type_to_opcode"

let icmp6_protochain (_protochain_num : string) (headers : packet_headers) =
  let sum_of_pred_headers = sum_of_header headers "ip6" in
  let protochain_num = int_of_string _protochain_num in
  let init_protochain =
    let dst_unreachable =
      if !Env.generate_protochain then
        Some {
          code = [
            Ldb (Offset (Exp (X, Lit sum_of_pred_headers, Addition)));
            St (Mem 15);
            Ld (Hexj (1 (* code*) + 1 (*type*) + 2 (* checksum *) + 4 (*unused*)));
            Add X;
            Tax;
            Ld (Mem 15);
          ];
          cond = Jeq (Hexj 1);
          jt = ret_true;
          jf = ret_false;
        }
      else
        Some {
          code = [
            Ldx (Hexj 48);
            Ldb (Off (sum_of_pred_headers + 40));
          ];
          cond = Jeq (Hexj 1);
          jt = ret_true;
          jf = ret_false;
        }
    in
    let pkt_too_big = Some {code = []; cond = Jeq (Hexj 2); jt = ret_true; jf = ret_false} in
    let time_exceeded = Some {code = []; cond = Jeq (Hexj 3); jt = ret_true; jf = ret_false;} in
    let param_prob = Some {code = []; cond = Jeq (Hexj 4); jt = ret_true; jf = ret_false} in
    let redirect =
      if !Env.generate_protochain then
        Some {
          code = [
            Ld (Hexj 40);
            Add X;
            Tax;
            Ld (Mem 15);
          ];
          cond = Jeq (Hexj 137);
          jt = ret_true;
          jf = ret_false;
        }
      else
        Some {
          code = [
            Ldx (Hexj (40 + 1 (* code*) + 1 (*type*) + 2 (* checksum *) + 4 (*unused*) + 16 + 16 + 8));
            Ldb (Off (sum_of_pred_headers + 40));
          ];
          cond = Jeq (Hexj 137);
          jt = ret_true;
          jf = ret_false;
        }
    in
    let icmp_ip6 =
      Some {
      code = [
        Ldb (Offset (Exp (X, Lit (sum_of_pred_headers + 6), Addition)));
        St (Mem 15);
        Ld (Hexj 40);
        Add X;
        Tax;
        Ld (Mem 15);
      ];
      cond = Jeq (Hexj protochain_num);
      jt = ret_true;
      jf = ret_false
    } in
    let icmp_errors =
      redirect
      |> disjoin param_prob
      |> disjoin time_exceeded
      |> disjoin pkt_too_big
      |> disjoin dst_unreachable
    in
    conjoin icmp_errors icmp_ip6
  in
  begin
    if !Config.max_rec > 0 then
      match List.assoc_opt protochain_num !protochain_asts with
      | None ->
        let recur = make_recur (!Config.max_rec) (generate_recursive_func sum_of_pred_headers protochain_num) in
        icmp6_protochain_asts := (protochain_num, disjoin init_protochain recur) :: !icmp6_protochain_asts;
      | _ -> ()
    else
      failwith "protochain must be used with -max_rec [n], n must be greater than 0"
  end;
  gen_icmp6_protochain protochain_num

let icmp6_to_sock_filter (icmp6_info : string list) (headers : packet_headers) =
  match icmp6_info with
  | "protochain" :: [protochain_num] ->
    Env.generate_icmp6_protochain := true;
    icmp6_protochain protochain_num headers
