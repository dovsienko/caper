(*
Copyright Hyunsuk Bang, April 2023

This file is part of Caper.

Caper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Caper is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Caper.  If not, see <https://www.gnu.org/licenses/>.

This module: register type definitions
*)

open Inst
open Block

type register =
  | NilReg
  (* | Reg of value * int literal is Hex n, 4 *)
  | Reg of value
  | Reg_exp of register * register * operator

type reg_cond =
  | NilCond
  | Eq of register
  | Neq of register
  | Lt of register
  | Le of register
  | Gt of register
  | Ge of register

let rec reg_to_str (r : register) : string =
  match r with
  | NilReg -> "Nil"
  | Reg value -> Printf.sprintf "%s" (value_to_str value)
  | Reg_exp (v1, v2, op) -> Printf.sprintf "(%s %s %s)" (reg_to_str v1) (operator_to_str op) (reg_to_str v2)

let reg_exp (r1 : register) (r2 : register) (op : operator) : register = Reg_exp (r1, r2, op)

let rec get_val (reg : register) : value =
  match reg with
  | NilReg -> NilVal
  | Reg v -> v
  | Reg_exp (v1, v2, op) -> Exp (get_val v1, get_val v2, op)
  | _ -> failwith (Printf.sprintf "%s" (reg_to_str reg))

let reg_value reg =
  match reg with
  | Reg v -> v

let addition reg1 reg2 =
  match reg1, reg2  with
  | Reg (Hex n1), Reg (Hex n2) -> Reg (Hex (n1 + n2))
  | _, _ -> Reg_exp (reg1, reg2, Addition)

let subtraction reg1 reg2 =
  match reg1, reg2  with
  | Reg (Hex n1), Reg (Hex n2) -> Reg (Hex (n1 - n2))
  | _, _ -> Reg_exp (reg1, reg2, Subtraction)

let multiplication reg1 reg2 =
  match reg1, reg2  with
  | Reg (Hex n1), Reg (Hex n2) -> Reg (Hex (n1 * n2))
  | _, _ -> Reg_exp (reg1, reg2, Multiplication)

let division reg1 reg2 =
  match reg1, reg2  with
  | Reg (Hex n1), Reg (Hex n2) -> Reg (Hex (n1 / n2))
  | _, _ -> Reg_exp (reg1, reg2, Division)

let modulo reg1 reg2 =
  match reg1, reg2  with
  | Reg (Hex n1), Reg (Hex n2) -> Reg (Hex (n1 mod n2))
  | _, _ -> Reg_exp (reg1, reg2, Modulo)

let arith_or reg1 reg2 =
  match reg1, reg2  with
  | Reg (Hex n1), Reg (Hex n2) -> Reg (Hex (n1 lor n2))
  | _, _ -> Reg_exp (reg1, reg2, Arith_or)

let arith_and reg1 reg2 =
  match reg1, reg2  with
  | Reg (Hex n1), Reg (Hex n2) -> Reg (Hex (n1 land n2))
  | _, _ -> Reg_exp (reg1, reg2, Arith_and)

let arith_xor reg1 reg2 =
  match reg1, reg2  with
  | Reg (Hex n1), Reg (Hex n2) -> Reg (Hex (n1 lxor n2))
  | _, _ -> Reg_exp (reg1, reg2, Arith_xor)

let logical_shift_left reg1 reg2 =
  match reg1, reg2  with
  | Reg (Hex n1), Reg (Hex n2) -> Reg (Hex (n1 lsl n2))
  | _, _ -> Reg_exp (reg1, reg2, Logical_shift_left)

let logical_shift_right reg1 reg2 =
  match reg1, reg2  with
  | Reg (Hex n1), Reg (Hex n2) -> Reg (Hex (n1 lsr n2))
  | _, _ -> Reg_exp (reg1, reg2, Logical_shift_right)

let rec regify (a_reg : register) (x_reg : register) = function
  | Lit n | Hex n | Hexj n -> Reg (Hex n)
  | Off n -> Reg (Off n)
  | Offset n -> Reg (Offset (get_val (regify a_reg x_reg n)))
  | X -> x_reg
  | Exp (v1, v2, op) ->
    match op with
    | Addition -> addition (regify a_reg x_reg v1) (regify a_reg x_reg v2)
    | Subtraction -> subtraction (regify a_reg x_reg v1) (regify a_reg x_reg v2)
    | Multiplication -> multiplication (regify a_reg x_reg v1) (regify a_reg x_reg v2)
    | Division -> division (regify a_reg x_reg v1) (regify a_reg x_reg v2)
    | Modulo -> modulo (regify a_reg x_reg v1) (regify a_reg x_reg v2)
    | Arith_or -> arith_or (regify a_reg x_reg v1) (regify a_reg x_reg v2)
    | Arith_and -> arith_and (regify a_reg x_reg v1) (regify a_reg x_reg v2)
    | Arith_xor -> arith_xor (regify a_reg x_reg v1) (regify a_reg x_reg v2)
    | Logical_shift_left -> logical_shift_left (regify a_reg x_reg v1) (regify a_reg x_reg v2)
    | Logical_shift_right -> logical_shift_right (regify a_reg x_reg v1) (regify a_reg x_reg v2)

let memory = Array.make 16 NilReg

let c_to_str (c : reg_cond) =
  match c with
  | NilCond -> ""
  | Eq value -> Printf.sprintf "== (%s)" (reg_to_str value)
  | Neq value -> Printf.sprintf "!= (%s)" (reg_to_str value)
  | Lt value -> Printf.sprintf "< (%s)" (reg_to_str value)
  | Le value -> Printf.sprintf "<= (%s)" (reg_to_str value)
  | Gt value -> Printf.sprintf "> (%s)" (reg_to_str value)
  | Ge value -> Printf.sprintf ">= (%s)" (reg_to_str value)

let negate_reg_cond c =
  match c with
  | a, x, NilCond -> a, x, NilCond
  | a, x, Eq value -> a, x, Neq value
  | a, x, Neq value -> a, x, Eq value
  | a, x, Lt value -> a, x, Ge value
  | a, x, Le value -> a, x, Gt value
  | a, x, Gt value -> a, x, Le value
  | a, x, Ge value -> a, x, Lt value

let cond_to_str (cond) =
  match cond with
  | a, x, c -> Printf.sprintf "X:%s A:%s %s" (reg_to_str x) (reg_to_str a) (c_to_str c)

type cond_collision =
  | No_Collision
  | Triv_True
  | Triv_False

let rec if_insert cs c =
  match cs with
  | [] -> No_Collision
  | hd :: tl ->
    match hd, c with
    | (a1, x1, cond1), (a2, x2, cond2) ->
      if a1 <> a2 then if_insert tl c
      else
        match cond1, cond2 with
        | NilCond, _ -> if_insert tl c
        | _, NilCond -> if_insert tl c
        | Eq x, Eq y -> if x = y then Triv_True else Triv_False
        | Eq x, Neq y -> if x = y then Triv_False else Triv_True
        | Neq x, Eq y -> if x = y then Triv_False else if_insert tl c
        | Neq x, Neq y -> if x = y then Triv_True else if_insert tl c
        | Eq x, Gt y -> if x > y then Triv_True else Triv_False
        | Eq x, Ge y -> if x >= y then Triv_True else Triv_False
        | Gt x, Gt y -> if x >= y then Triv_True else if_insert tl c
        | Gt x, Ge y -> if x > y then Triv_True else if_insert tl c
        | Ge x, Gt y -> if x > y then Triv_True else if_insert tl c
        | Ge x, Ge y -> if x >= y then Triv_True else if_insert tl c

let get_a_reg = fun (a,_, _) -> a
let get_x_reg = fun (_, x, _) -> x

let rec eval_opcode a_reg x_reg opcode =
  match opcode with
  | No_op _ | Nil_op  -> a_reg, x_reg
  | St (Mem n) ->
    begin
      memory.(n) <- a_reg;
      a_reg, x_reg
    end
  | Stx (Mem n) ->
    begin
      memory.(n) <- x_reg;
      a_reg, x_reg
    end
  | Ldi (Lit n) | Ldi (Hex n) | Ldi (Hexj n) -> (Reg (Hex n)), x_reg
  | Ld (Lit n)  | Ld (Hex n)  | Ld (Hexj n) -> (Reg (Hex n)), x_reg
  | Ldx (Lit n) | Ldx (Hex n) | Ldx (Hexj n) -> a_reg, (Reg (Hex n))
  | Ld (Off n) -> (Reg (Off n)), x_reg
  | Ldh (Off n)  ->  (Reg (Off n)), x_reg
  | Ldb (Off n)  ->  (Reg (Off n)), x_reg
  | Ldxb (Off n) ->  a_reg, (Reg (Off n))
  | Ldx (Off n) ->  a_reg, (Reg (Off n))
  | Ld (Mem n)   ->  memory.(n), x_reg
  | Ldh (Mem n)  ->  memory.(n), x_reg
  | Ldb (Mem n)  ->  memory.(n), x_reg
  | Ldx (Mem n)  ->  a_reg, memory.(n)
  | Ldxb (Mem n) ->  a_reg, memory.(n)
  | Ld (Offset x)  ->  (regify a_reg x_reg (Offset x)), x_reg
  | Ldh (Offset x) ->  (regify a_reg x_reg (Offset x)), x_reg
  | Ldb (Offset x) ->  (regify a_reg x_reg (Offset x)), x_reg
  | Ldxb (Exp (v1,v2,op)) ->  a_reg, (regify a_reg x_reg (Exp (v1,v2,op)))
  | Add (Lit n) | Add (Hex n) | Add (Hexj n) ->  (addition a_reg (Reg (Hex n))), x_reg
  | Sub (Lit n) | Sub (Hex n) | Sub (Hexj n) ->  (subtraction a_reg (Reg (Hex n))), x_reg
  | Mul (Lit n) | Mul (Hex n) | Mul (Hexj n) ->  (multiplication a_reg (Reg (Hex n))), x_reg
  | Div (Lit n) | Div (Hex n) | Div (Hexj n) ->  (division a_reg (Reg (Hex n))), x_reg
  | Mod (Lit n) | Mod (Hex n) | Mod (Hexj n) ->  (modulo a_reg (Reg (Hex n))), x_reg
  | And (Lit n) | And (Hex n) | And (Hexj n) ->  (arith_and a_reg (Reg (Hex n))), x_reg
  | Or (Lit n)  | Or (Hex n)  | Or (Hexj n)  ->  (arith_or a_reg (Reg (Hex n))), x_reg
  | Xor (Lit n) | Xor (Hex n) | Xor (Hexj n) ->  (arith_xor a_reg (Reg (Hex n))), x_reg
  | Lsh (Lit n) | Lsh (Hex n) | Lsh (Hexj n) ->  (logical_shift_left a_reg (Reg (Hex n))), x_reg
  | Rsh (Lit n) | Rsh (Hex n) | Rsh (Hexj n) ->  (logical_shift_right a_reg (Reg (Hex n))), x_reg
  | Add X ->  (addition a_reg x_reg), x_reg
  | Sub X ->  (subtraction a_reg x_reg), x_reg
  | Mul X ->  (multiplication a_reg x_reg), x_reg
  | Div X ->  (division a_reg x_reg), x_reg
  | Mod X ->  (modulo a_reg x_reg), x_reg
  | And X ->  (arith_and a_reg x_reg), x_reg
  | Or X  ->  (arith_or a_reg x_reg), x_reg
  | Xor X ->  (arith_xor a_reg x_reg), x_reg
  | Lsh X ->  (logical_shift_left a_reg x_reg), x_reg
  | Rsh X ->  (logical_shift_right a_reg x_reg), x_reg
  | Tax ->  a_reg, a_reg
  | Txa ->  x_reg, x_reg
  | Ret False | Ret True ->  a_reg, x_reg

let rec eval_opcodes a_reg x_reg opcodes jump_cond =
  match opcodes with
  | [] ->
    begin
      match jump_cond with
      | Jeq (Lit n) | Jeq (Hex n) | Jeq (Hexj n) -> a_reg, x_reg, (Eq (Reg (Hex n)))
      | Jneq (Lit n) | Jneq (Hex n) | Jneq (Hexj n) -> a_reg, x_reg, (Neq (Reg (Hex n)))
      | Jne (Lit n) | Jne (Hex n) | Jne (Hexj n) -> a_reg, x_reg, (Neq (Reg (Hex n)))
      | Jlt (Lit n) | Jlt (Hex n) | Jlt (Hexj n) -> a_reg, x_reg, (Lt (Reg (Hex n)))
      | Jle (Lit n) | Jle (Hex n) | Jle (Hexj n) -> a_reg, x_reg, (Le (Reg (Hex n)))
      | Jgt (Lit n) | Jgt (Hex n) | Jgt (Hexj n) -> a_reg, x_reg, (Gt (Reg (Hex n)))
      | Jge (Lit n) | Jge (Hex n) | Jge (Hexj n) -> a_reg, x_reg, (Ge (Reg (Hex n)))
      | Jset (Lit n) | Jset (Hex n) | Jset (Hexj n) -> (reg_exp a_reg (Reg (Hex n)) Arith_and), x_reg, (Eq (Reg (Hex 0)))
      | Jeq X -> a_reg, x_reg, (Eq x_reg)
      | Jneq X -> a_reg, x_reg, (Neq x_reg)
      | Jne X -> a_reg, x_reg, (Neq x_reg)
      | Jlt X -> a_reg, x_reg, (Lt x_reg)
      | Jle X -> a_reg, x_reg, (Le x_reg)
      | Jgt X -> a_reg, x_reg, (Gt x_reg)
      | Jge X -> a_reg, x_reg, (Ge x_reg)
      | Jset X -> a_reg, x_reg, (Eq x_reg)
      | Ip_Protochain_cond (Lit n) -> a_reg, x_reg, NilCond
      | Ip6_Protochain_cond (Lit n) -> a_reg, x_reg, NilCond
      | Icmp6_Protochain_cond (Lit n) -> a_reg, x_reg, NilCond
      | Ip_Protochain_Bridge_cond (Lit n) -> a_reg, x_reg, NilCond
      | Ip6_Protochain_Bridge_cond (Lit n) -> a_reg, x_reg, NilCond
      | Icmp6_Protochain_Bridge_cond (Lit n) -> a_reg, x_reg, NilCond
    end
  | head :: rest ->
    let regs = eval_opcode a_reg x_reg head in
    eval_opcodes (fst regs) (snd regs) rest jump_cond
