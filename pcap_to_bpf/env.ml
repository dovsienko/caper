(*
  Copyright Hyunsuk Bang, March 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: Environment Variables for BPF compilation 
*)

let generate_protochain = ref false
let generate_icmp6_protochain = ref false
let protochain_list = ref []

let append_protochain (n : int) =
  protochain_list := (n :: !protochain_list)

let reset_env () =
  begin
    generate_protochain := false;
    generate_icmp6_protochain := false;
    protochain_list := [];
  end
