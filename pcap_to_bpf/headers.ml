(*
  Copyright Hyunsuk Bang, February 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: Header type definition for context
*)

type layer_2 =
  | Ether
  | Fddi

type layer_2_5 =
  | Mpls
  | Vlan

type layer_3 =
  | Ip
  | Ip6
  | Arp
  | Rarp

type layer_4 =
  | Icmp (* for the sake of simplicity ICMP and ICMP6 are considered as layer4 since it works on top of IP and IPv6*)
  | Icmp6
  | Tcp
  | Udp
  | Sctp

type protocol =
  | L2 of layer_2
  | L2_5 of layer_2_5
  | L3 of layer_3
  | L4 of layer_4

type packet_headers =
  | Nil
  | Frame of protocol * (protocol list)
  | Packet of protocol * (protocol list) * protocol
  | Segment of protocol * (protocol list) * protocol * protocol

let string_to_protocol = function
  | "ether" -> L2 Ether
  | "fddi" -> L2 Fddi
  | "vlan" -> L2_5 Vlan
  | "mpls" -> L2_5 Mpls
  | "ip" -> L3 Ip
  | "ip6" -> L3 Ip6
  | "icmp" -> L4 Icmp
  | "icmp6" -> L4 Icmp6
  | "arp" -> L3 Arp
  | "rarp" -> L3 Rarp
  | "tcp" -> L4 Tcp
  | "udp" -> L4 Udp
  | "sctp" -> L4 Sctp
  | _ -> failwith "unsupported Layer"

let protocol_to_string = function
  | L2 Ether -> "ether"
  | L2 Fddi -> "fddi"
  | L2_5 Vlan -> "vlan"
  | L2_5 Mpls -> "mpls"
  | L3 Ip -> "ip"
  | L3 Ip6 -> "ip6"
  | L3 Arp -> "arp"
  | L3 Rarp -> "rarp"
  | L4 Icmp -> "icmp"
  | L4 Icmp6 -> "icmp6"
  | L4 Tcp -> "tcp"
  | L4 Udp -> "udp"
  | L4 Sctp -> "sctp"
  | _ -> failwith "unsupported Layer"

let get_protocol_size (p : protocol) =
  match p with
  | L2 Ether -> 14
  | L2_5 Vlan -> 4
  | L2_5 Mpls -> 4
  | L2 Fddi -> 14
  | L3 Ip -> 20
  | L3 Ip6 -> 40
  | L4 Icmp -> 8
  | L4 Icmp6 -> 8
  | L3 Arp -> 28
  | L3 Rarp -> 28

let headers_to_string (headers : packet_headers) =
  let rec l2_5_to_string l2_5_layers =
    match l2_5_layers with
    | [] -> ""
    | L2_5 Vlan :: ls -> "vlan :" ^ l2_5_to_string ls
    | L2_5 Mpls :: ls -> "mpls : " ^ l2_5_to_string ls
    | _ -> failwith "Headers.l2_5_to_string"
  in
  match headers with
   | Nil -> ""
   | Frame (l2, l2_5) -> Printf.sprintf "[%s][%s]" (protocol_to_string l2) (l2_5_to_string l2_5)
   | Packet (l2, l2_5 , l3) -> Printf.sprintf "[%s][%s][%s]" (protocol_to_string l2) (l2_5_to_string l2_5) (protocol_to_string l3)
   | Segment (l2, l2_5, l3, l4) -> Printf.sprintf "[%s][%s][%s][%s]" (protocol_to_string l2) (l2_5_to_string l2_5) (protocol_to_string l3) (protocol_to_string l4)

let encapsulate (headers : packet_headers) (protocol : string) : packet_headers =
  let proto = string_to_protocol protocol in
  match headers, proto with
  | Nil, L2_5 Vlan -> Frame (L2 Ether, [L2_5 Vlan])
  | Nil, L2_5 Mpls -> Frame (L2 Ether, [L2_5 Mpls])
  | Nil, L2 Ether -> Frame (L2 Ether, [])
  | Nil, L3 Ip -> Packet (L2 Ether, [], L3 Ip) (* FIXME *)
  | Nil, L3 Ip6 -> Packet (L2 Ether, [], L3 Ip6) (* FIXME *)
  | Nil, L3 Arp -> Packet (L2 Ether, [], L3 Arp) (* FIXME *)
  | Nil, L3 Rarp -> Packet (L2 Ether, [], L3 Rarp) (* FIXME *)
  | Frame (l2, l2_5) , L2_5 Vlan -> Frame (l2, L2_5 Vlan :: l2_5)
  | Frame (l2, l2_5), L2_5 Mpls -> Frame (l2, L2_5 Mpls :: l2_5)
  | Frame (l2, _), L2 Ether ->
    if l2 = L2 Ether then
      headers
    else
      failwith (Printf.sprintf "bad encapsulation,%s <- %s" (headers_to_string headers) (protocol_to_string proto))
  | Frame (l2, l2_5), L3 x -> Packet (l2, l2_5, L3 x)
  | Frame _, L4 x -> failwith (Printf.sprintf "bad encapsulation,%s <- %s" (headers_to_string headers) (protocol_to_string proto))
  | Packet (l2, l2_5, l3), L2 x ->
    if l2 = L2 x then
      headers
    else
      failwith (Printf.sprintf "bad encapsulation,%s <- %s" (headers_to_string headers) (protocol_to_string proto))
  | Packet _, L2_5 Vlan -> Frame (L2 Ether, [L2_5 Vlan]) (* new context !*)
  | Packet (l2, l2_5, l3), L3 x ->
    if l3 = L3 x then
      headers
    else
      failwith (Printf.sprintf "bad encapsulation,%s <- %s" (headers_to_string headers) (protocol_to_string proto))
  | Packet (l2, l2_5, l3), L4 x -> Segment (l2, l2_5, l3, L4 x)
  | Segment (l2, l2_5, l3, l4), L2 x ->
    if l2 = L2 Ether then
      headers
    else
      failwith (Printf.sprintf "bad encapsulation,%s <- %s" (headers_to_string headers) (protocol_to_string proto))
  | Segment _, L2_5 Vlan -> Frame (L2 Ether, [L2_5 Vlan]) (* new context !*)
  | Segment (l2, l2_5, l3, l4), L3 x ->
    if l3 = L3 x then
      headers
    else
      failwith (Printf.sprintf "bad encapsulation,%s <- %s" (headers_to_string headers) (protocol_to_string proto))
  | Segment (l2, l2_5, l3, l4), L4 x ->
    if l4 = L4 x then
      headers
    else
      failwith (Printf.sprintf "bad encapsulation,%s <- %s" (headers_to_string headers) (protocol_to_string proto))
  | _ -> failwith "encapsulation error!"

let get_l2 (headers : packet_headers) : protocol =
  match headers with
  | Frame (x, _) | Packet (x, _ , _) | Segment (x, _, _, _) -> x
  | _ -> failwith "cannot extract L2 layer from headers"

let get_l2_5 (headers : packet_headers) : protocol list =
  match headers with
  | Frame (_, x) | Packet (_, x , _) | Segment (_, x, _, _) -> x
  | _ -> failwith "cannot extract L2.5 layer from headers"

let get_l3 (headers : packet_headers) : protocol =
  match headers with
  | Packet (_, _, x) | Segment (_, _, x, _) -> x
  | Nil | Frame _ -> failwith "cannot extract L3 layer from headers"

let get_l4 (headers : packet_headers) : protocol =
  match headers with
  | Segment (_, _, x, _) -> x
  | Nil | Frame _ | Packet _ -> failwith "cannot extract L3 layer from headers"

let get_l2_5_size (headers : packet_headers) : int =
  match headers with
  | Frame (_, x) | Packet (_, x, _) | Segment (_, x, _, _) ->  List.fold_left ( + ) 0 (List.map get_protocol_size x)
  | _ -> failwith "cannot extract L2 layer from headers"

let have_mpls (headers : packet_headers) : bool =
  let rec have_mpls_helper (l2_5 : protocol list) : bool =
    match l2_5 with
    | [] -> false
    | L2_5 Mpls  :: l2_5s -> true
    | _ :: l2_5s -> have_mpls_helper l2_5s
  in
  have_mpls_helper (get_l2_5 headers)

(* sum of all the headers below the target layer*)
let sum_of_header (headers : packet_headers) (target : string) : int =
  let proto = string_to_protocol target in
  match headers, proto with
  | Frame (l2, l2_5), L2 x -> 0
  | Packet (l2, l2_5, l3), L3 x | Segment (l2, l2_5, l3, _), L3 x ->
    if l3 = L3 x then
      get_protocol_size l2 + get_l2_5_size headers
    else
      failwith (Printf.sprintf "Headers.sum_of_headers: Wrong context! Current context is %s but given: %s" (headers_to_string headers) target)
  | Segment (l2, l2_5, l3, l4), L4 x ->
    if l4 = L4 x then
      get_protocol_size l2 + get_l2_5_size headers + get_protocol_size l3
    else
      failwith (Printf.sprintf "Headers.sum_of_headers: Wrong context! Current context is %s but given: %s" (headers_to_string headers) target)
  | _ -> failwith "unsupported packet_headers"