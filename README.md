![Caper](doc/small_caper.jpg)


# Caper
Caper is a tool for understanding and processing "pcap expressions" (also known as *tcpdump filters*) which are used for [network packet analysis](https://en.wikipedia.org/wiki/Packet_analyzer).
Caper can be used for:
* Expanding out pcap expressions "in full" to understand their implicit features.
* Reasoning about whether two expressions accept the same set of packets, or how their accepted packets differ.
* Converting pcap expressions into BPF programs.
* Converting between pcap expressions and English.

More info can be found in [the Caper paper](https://www.nik.network/caper/pcap_semantics.pdf).


# Building
Run `./build.sh`.

Instead of building in your immediate environment, you can use the
[Vagrantfile](Vagrantfile) that sets up a Debian 11 VM with the dependencies
that are needed to compile Caper, and then compiles it for you in that VM.
There is also a [Dockerfile](Dockerfile).


# Usage
Once Caper is compiled, running `./caper.byte` shows you a summary of command-line parameter behavior.
We document several [usage examples](EXAMPLES.md) of Caper.


# License
Caper is released under [GPLv3 or any later version](COPYING)


# Contributors
[Nik Sultana](https://www.nik.network/)  
[Hyunsuk Bang](https://hyunsuk-bang.github.io/resume/index.html)  
Denis Ovsienko  
[Marelle Leon](https://gitlab.com/marellec)  
Jan Viktorin  
[Aditi Kumar](https://gitlab.com/aditi1421)  

See this [list of ideas of how to contribute to Caper](http://caper.cs.iit.edu/#contributing).
