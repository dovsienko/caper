(*
  Copyright Marelle Leon, March 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Pcap spec for translating English expressions to pcap
*)

open Angstrom
open English_syntax
open English_parsing
open Pcap_syntax

type pcap_field_keyword =
  | TypField of typ
  | DirField of dir
  | ProtoField of proto

let protos =
  [
    ("ether", Ether);
    ("ethernet", Ether);
    (* ("ip", Ip); *)
    ("ip4", Ip);
    ("ipv4", Ip);
    ("IPV4", Ip);
    ("IPv4", Ip);
    ("ip6", Ip6);
    ("ipv6", Ip6);
    ("IPV6", Ip6);
    ("IPv6", Ip6);
    ("arp", Arp);
    ("rarp", Rarp);
    ("tcp", Tcp);
    ("udp", Udp);
    ("sctp", Sctp);
    ("vlan", Vlan);
    ("mpls", Mpls);
    ("icmp", Icmp);
    ("icmp6", Icmp6);
  ]

let dirs =
  [
    ("src", Src);
    ("source", Src);
    ("dst", Dst);
    ("destination", Dst);
    ("src or dst", Src_or_dst);
    ("source or destination", Src_or_dst);
    ("src and dst", Src_and_dst);
    ("source and destination", Src_and_dst);
    ("inbound", Inbound);
    ("outbound", Outbound);
    ("broadcast", Broadcast);
    ("multicast", Multicast);
  ]

let typs =
  [
    ("host", Host);
    ("hostname", Host);
    ("net", Net);
    ("network", Net);
    ("port", Port_type);
    ("port name", Port_type);
    ("portrange", Portrange);
    ("port range", Portrange);
    ("proto", Proto);
    ("protochain", Protochain);
    ("gateway", Gateway);
    (* ("less", Less);
       ("greater", Greater) *)
  ]

let value_required_dirs =
  [
    ("src", Src);
    ("source", Src);
    ("dst", Dst);
    ("destination", Dst);
    ("src or dst", Src_or_dst);
    ("source or destination", Src_or_dst);
    ("src and dst", Src_and_dst);
    ("source and destination", Src_and_dst);
  ]

let valueless_dirs = [ ("broadcast", Broadcast); ("multicast", Multicast) ]
let standalone_dirs = [ ("inbound", Inbound); ("outbound", Outbound) ]

let proto_keyword_of_engl_string x =
  Option.map (fun y -> ProtoField y) (List.assoc_opt x protos)

let dir_keyword_of_engl_string x =
  Option.map (fun y -> DirField y) (List.assoc_opt x dirs)

let typ_keyword_of_engl_string x =
  Option.map (fun y -> TypField y) (List.assoc_opt x typs)

let pcap_engl_phrase_typs = List.map fst typs
let pcap_engl_phrase_value_required_dirs = List.map fst value_required_dirs
let pcap_engl_phrase_valueless_dirs = List.map fst valueless_dirs
let pcap_engl_phrase_standalone_dirs = List.map fst standalone_dirs
let pcap_engl_phrase_protos = List.map fst protos

let pcap_proto_phrase =
  make_phrase
    (match_first_phrase
      (token_lists_of_phrases_by_length pcap_engl_phrase_protos))

let pcap_value_required_dir_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length pcap_engl_phrase_value_required_dirs))

let pcap_valueless_dir_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length pcap_engl_phrase_valueless_dirs))

let pcap_standalone_dir_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length pcap_engl_phrase_standalone_dirs))

let pcap_typ_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length pcap_engl_phrase_typs))

let pcap_dir_typ_phrase_sequence =
  pcap_value_required_dir_phrase
  >>= (fun d -> pcap_typ_phrase >>= fun t -> return [ d; t ])
  <|> (pcap_value_required_dir_phrase >>= fun d -> return [ d ])
  <|> (pcap_typ_phrase >>= fun t -> return [ t ])

let pcap_with_mask_phrase = with_clause "mask"
let pcap_of_length_phrase = of_size_clause "length" "bits"

let pcap_packet_access_phrase =
  ordinal_collection_expression
    [ "byte"; "2 bytes"; "4 bytes"; "word" ]
    pcap_engl_phrase_protos "header"

let pcap_relation_expression = relation_expression [ pcap_packet_access_phrase ]

let pcap_examining_relation_expression =
  examining_relation_expression [ pcap_packet_access_phrase ]

let pcap_is_that_of_expression =
  is_that_of_expression pcap_dir_typ_phrase_sequence
    [
      pcap_with_mask_phrase;
      pcap_of_length_phrase;
      pcap_typ_phrase;
      pcap_proto_phrase;
      unidentified_term;
    ]

let pcap_that_has_expression =
  that_has_expression pcap_proto_phrase pcap_dir_typ_phrase_sequence
    [
      pcap_with_mask_phrase;
      pcap_of_length_phrase;
      pcap_typ_phrase;
      pcap_proto_phrase;
      unidentified_term;
    ]

let pcap_that_is_expression =
  that_is_expression pcap_proto_phrase
    [ pcap_valueless_dir_phrase; unidentified_term ]

let pcap_of_type_expression =
  of_type_expression [ pcap_proto_phrase; pcap_standalone_dir_phrase ]

let pcap_english_expression =
  english_expression
    [
      pcap_is_that_of_expression;
      pcap_that_has_expression;
      pcap_that_is_expression;
      pcap_of_type_expression;
      pcap_relation_expression;
      pcap_examining_relation_expression;
      boolean_atom_expression;
    ]

(** parse a pcap english expression into an English AST *)
let parse_engl_expression_of_pcap_expression =
  parse_string ~consume:Prefix pcap_english_expression
