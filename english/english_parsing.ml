(*
  Copyright Marelle Leon, March 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Parsing for English expressions
*)

open Angstrom
open English_syntax

(* combinators *)

(** fails string parser p only on empty strings *)
let nonempty p =
  p >>= fun s ->
  if s = "" then fail "empty string in 'nonempty' parser" else return s

(** returns a parser with given parser's list of strings concatenated by a space *)
let stringify p = p >>= fun strs -> return (String.concat " " strs)

(** 
   recurses with a new parser (returned from next_p_funct)
    - on each iteration

   begins recursion by passing base_val into next_p_funct
    - after this, passes current parser's output into next_p_funct 
   
   returns a parser that holds the final value

   recursion ends when next_p_funct returns None

   if the current parser fails, failure will propogate
*)
let recurse_parser (next_p_funct : 'a -> 'a option t) (base_val : 'a) =
  let rec recurse arg_val =
    next_p_funct arg_val >>= function
    | None -> return arg_val
    | Some p_output_val -> recurse p_output_val
  in
  recurse base_val

(**
   always succeeds

   given a stopping point, returns a list of 
    - (all the consecutive matches)
      before stopping point is matched
      OR end of input is reached

   only advances input to (right before stop point)
*)
let rest_of_matches_before p stop_point =
  many
    ( stop_point *> return true <|> return false >>= fun reached_stop_point ->
      if reached_stop_point then fail "stop point reached" else p )
  >>= fun rest -> return rest

(**
   always succeeds

   returns a list of 
   (all the consecutive matches before end of input is reached)
*)
let rest_of_matches p = rest_of_matches_before p (fail "")

(* English syntax *)

let make_phrase p = p >>= fun ph -> return (Phrase ph)

(* characters *)

let is_whitespace = function
  | '\x20' | '\x0a' | '\x0d' | '\x09' -> true
  | _ -> false

let whitespace = skip_while is_whitespace
let is_digit = function '0' .. '9' -> true | _ -> false
let is_any_tok x = not (is_whitespace x)

let is_arith_tok x =
  is_any_tok x && (not (x = '#')) && (not (x = '{')) && not (x = '}')

let is_regular_tok x =
  is_arith_tok x
  && (not (x = ','))
  && (not (x = ':'))
  && (not (x = '('))
  && (not (x = ')'))
  && (not (x = '['))
  && not (x = ']')

let is_value_tok x = is_regular_tok x || x = ':'

(* tokens *)

let token p = whitespace *> p <* whitespace
let any_token = token (take_while1 is_any_tok)
let arith_token = token (take_while1 is_arith_tok)
let regular_token = token (take_while1 is_regular_tok)
let value_token = token (take_while1 is_value_tok)

(**
   if the next char is the given ch,
    - goes forward one char and returns it in a string
   else,
    - stays in the same place and returns an empty string 
*)
let advance_char ch =
  peek_char >>= function
  | Some c when c = ch -> advance 1 *> return (String.make 1 ch)
  | _ -> return ""

let open_paren = token (advance_char '(')
let close_paren = token (advance_char ')')
let open_brace = token (advance_char '{')
let close_brace = token (advance_char '}')
let open_bracket = token (advance_char '[')
let close_bracket = token (advance_char ']')
let comma = token (advance_char ',')
let colon = token (advance_char ':')
let hashtag = token (advance_char '#')

(* phrases   (string combinators) *)

(** 
    phrase token lists must be sorted by length in descending order
    
    so that the longest possible matching phrase is caught

    - (length = number of tokens in phrase)
*)
let token_lists_of_phrases_by_length s =
  List.map (String.split_on_char ' ') s
  |> List.sort (fun x y -> compare (List.length y) (List.length x))

(** accepts the phrase (given as a list of tokens) *)
let keytoks (toks : string list) =
  let rec consume_keyphrase = function
    | [] -> return (String.concat " " toks)
    | t :: ts ->
        regular_token >>= fun next_t ->
        if next_t = t then consume_keyphrase ts
        else
          fail
            ("'" ^ next_t ^ "'" ^ " doesn't match '" ^ t ^ "' in phrase: '"
           ^ String.concat " " toks ^ "'")
  in
  consume_keyphrase toks

(** 
   accepts the first matching phrase from given list of phrases 
   (each in token list form) 
*)
let match_first_phrase phrase_tokens =
  List.map keytoks phrase_tokens |> choice ~failure_msg:"no phrases matched"

(** accepts the given keyphrase *)
let keyphrase ph_s =
  keytoks (String.split_on_char ' ' ph_s)
  <|> fail ("missing keyphrase '" ^ ph_s ^ "'")

(** accepts the given keyword *)
let keyword keyword_s =
  token (Angstrom.string keyword_s)
  <|> fail ("missing keyword '" ^ keyword_s ^ "'")

(* expressions *)

(**
   returns a Clause of an UnidentifiedString of the rest of the input
   (until the given stop point)
*)
let rest_of_expression_before stop_point =
  rest_of_matches_before any_token stop_point >>= fun ts ->
  return (Clause (Unit (UnidentifiedString (String.concat " " ts))))

(**
  always succeeds 

  the rest of the input
*)
let rest_of_expression = rest_of_expression_before (fail "")

(** next regular_token *)
let unidentified_term =
  value_token >>= fun next_token -> return (UnidentifiedString next_token)

(** "true" / "false" *)
let boolean_atom_expression =
  keyword "true" *> return True_expr
  <|> keyword "false" *> return False_expr
  <|> fail "no 'true'/'false' keyword"

(**
   accepts one expression, followed by 0 or more consecutive
      - (separator >>= prefix >>= expression),

   returns a singleton expression 
   (or a combined expression using combine_exprs)

   fails when not all separators match (not uniform)
*)
let uniform_separated_expression (singleton_allowed : bool) (inner_expr : 'e t)
    (separators : 'sep t list) (combine_exprs : 'sep -> 'e list -> 'e) =
  (**
     returns an optional result of
      - Ok (uniform value) - when all values are uniform
      - Error (uniform value, first unmatching value) - otherwise

    returns None on empty list
  *)
  let uniform_value_of_list lst =
    List.fold_right
      (fun item uniform_rest ->
        match uniform_rest with
        | Ok opt -> (
            match opt with
            | Some u -> if item = u then uniform_rest else Error (u, item)
            | None -> Ok (Some item))
        | Error _ -> uniform_rest)
      lst (Ok None)
  in
  let separator = choice separators in
  inner_expr >>= fun e1 ->
  many
    ( separator >>= fun sep ->
      inner_expr >>= fun e -> return (sep, e) )
  >>= fun sep_es ->
  let seps, es = (List.map fst sep_es, List.map snd sep_es) in
  match uniform_value_of_list seps with
  | Ok opt -> (
      match opt with
      (* singleton expression *)
      | None ->
          if singleton_allowed then return e1
          else fail "singleton found, but no singleton allowed"
      (* list of expressions *)
      | Some sep -> return (combine_exprs sep (e1 :: es)))
  | Error (_, _) -> fail "unmatching separator"

(** returns true/false based on matching positive/negative phrase *)
let pos_neg_keyphrase pos_phrases neg_phrases fail_msg =
  (* matches negative first because positive phrase can be a prefix of negative phrase *)
  match_first_phrase (token_lists_of_phrases_by_length neg_phrases)
  *> return false
  <|> match_first_phrase (token_lists_of_phrases_by_length pos_phrases)
      *> return true
  <|> fail fail_msg

let not_keyword_pos_neg = keyword "not" *> return false <|> return true

let is_keyword_pos_neg =
  pos_neg_keyphrase [ "is"; "is that of" ]
    [ "is not"; "is not that of" ]
    "no 'is that of' keyword"

let has_keyword_pos_neg =
  pos_neg_keyphrase [ "has" ] [ "does not have" ] "no 'has' keyword"

let is_one_of_keyword_pos_neg =
  pos_neg_keyphrase [ "is one of"; "is in" ]
    [ "is not one of"; "is not in" ]
    "no 'is one of' keyword"

let is_one_of_keyword_only_pos =
  is_one_of_keyword_pos_neg >>= fun logically_positive ->
  if logically_positive then return logically_positive
  else fail "'is one of' keyword must be positive"

let of_type_keyword_pos_neg =
  pos_neg_keyphrase [ "of type" ] [ "not of type" ] "no 'of type' keyword"

(*
    'the ___th  ___ of the ___'
*)
let ordinal_collection_expression ordinal_names collection_names
    collection_type_name =
  (**
    accepts an ordinal token
    and returns its translated digit string
  *)
  let digit_string_of_ordinal_token =
    (* FIXME doesn't reject 1th / 2th / 3th *)
    token
      (take_while1 is_digit <* Angstrom.string "th"
      <|> (match_first_phrase
             (token_lists_of_phrases_by_length
                [
                  "1st";
                  "2nd";
                  "3rd";
                  "first";
                  "second";
                  "third";
                  "fourth";
                  "fifth";
                  "sixth";
                  "seventh";
                  "eighth";
                ])
           >>= function
           | "1st" | "first" -> return "1"
           | "2nd" | "second" -> return "2"
           | "3rd" | "third" -> return "3"
           | "fourth" -> return "4"
           | "fifth" -> return "5"
           | "sixth" -> return "6"
           | "seventh" -> return "7"
           | "eighth" -> return "8"))
    <|> fail "cannot match ordinal (nth)"
  in
  (keyword "the" <|> fail "no 'the' in 'ordinal reference' expression")
  *> digit_string_of_ordinal_token
  >>= fun number ->
  match_first_phrase (token_lists_of_phrases_by_length ordinal_names)
  <|> fail "missing ordinal name in 'ordinal reference' expression"
  >>= fun ordinal_name ->
  (keyphrase "of the"
  <|> fail "missing 'of the' in 'ordinal reference' expression")
  *> (match_first_phrase (token_lists_of_phrases_by_length collection_names)
     <|> fail "missing collection name in 'ordinal reference' expression")
  >>= fun collection_name ->
  (keyphrase collection_type_name
  <|> fail
        ("missing keyphrase '" ^ collection_type_name
       ^ "' in 'ordinal reference' expression"))
  *> return
       (OrdinalReference
          (number, ordinal_name, collection_name, collection_type_name))

(**
    '{<arith expression>}' may contain a '# <ordinal reference expression>', 
    and may contain an '# it' if inside another clause 
    where '# it' is defined
*)
let arithmetic_expression ?(it_variable = None) inner_terms =
  let inner_term =
    nonempty hashtag
    *> ((match it_variable with
        | Some v -> keyword "it" *> return (true, v)
        | None ->
            fail
              ("can't have '# it' without defined variable in"
             ^ " 'arithmetic' expression"))
       <|> (choice inner_terms >>= fun v -> return (false, v)))
  in
  nonempty open_brace *> stringify (many arith_token) >>= fun start_arith_s ->
  let next_p (used_it_variable, variable_chunks) =
    inner_term
    >>= (fun (next_used_it_variable, left_inner_term) ->
          stringify (many arith_token) >>= fun right_arith_s ->
          return
            (Some
               ( used_it_variable || next_used_it_variable,
                 (left_inner_term, right_arith_s, used_it_variable)
                 :: variable_chunks )))
    <|> nonempty close_brace *> return None
    <|> fail "syntax error or missing closing brace in 'arithmetic' expression"
  in
  recurse_parser next_p (false, [])
  >>= fun (used_it_variable, variable_chunks) ->
  return (used_it_variable, Arithmetic (start_arith_s, List.rev variable_chunks))

(* FIXME make a 'not' version of this using logically_positive *)
(* FIXME separate 'used {it} variable' flag *)
(*
    ['{<arith expression>}' / 'the ___th  ___ of the ___'] ' ' 
    ['equals' / 'is less than' / ...] ' ' 
    ['{<arith expression>}' / 'the ___th  ___ of the ___']
*)
let relation_clause ?(it_variable = None) inner_terms =
  let comparators =
    [
      ("=", Phrase "=");
      ("!=", Phrase "!=");
      ("<", Phrase "<");
      (">", Phrase ">");
      ("<=", Phrase "<=");
      (">=", Phrase ">=");
      ("is", Phrase "=");
      ("equals", Phrase "=");
      ("is equal to", Phrase "=");
      ("is not equal to", Phrase "!=");
      ("does not equal", Phrase "!=");
      ("is less than", Phrase "<");
      ("is greater than", Phrase ">");
      ("is more than", Phrase ">");
      ("is less than or equal to", Phrase "<=");
      ("is greater than or equal to", Phrase ">=");
      ("is more than or equal to", Phrase ">=");
    ]
  in
  let arith_subexpr =
    (match it_variable with
    | Some v ->
        keyword "it" *> return (true, v)
        <|> (choice inner_terms >>= fun v -> return (false, v))
    | None -> choice inner_terms >>= fun v -> return (false, v))
    >>= (fun (used_it_variable, inner_term) ->
          return
            ( used_it_variable,
              Arithmetic ("", [ (inner_term, "", used_it_variable) ]) ))
    <|> arithmetic_expression ~it_variable inner_terms
  in
  arith_subexpr >>= fun (used_it_var_left, left_term) ->
  match_first_phrase
    (token_lists_of_phrases_by_length (List.map fst comparators))
  <|> fail "incorrect comparison keyphrase in relation expression"
  >>= fun comp ->
  arith_subexpr >>= fun (used_it_var_right, right_expr) ->
  return
    ( used_it_var_left || used_it_var_right,
      Relation_clause (List.assoc comp comparators, left_term, right_expr) )

let relation_expression ?(it_variable = None) inner_terms =
  not_keyword_pos_neg >>= fun logically_positive ->
  ((if logically_positive then return ""
   else
     nonempty open_paren
     <|> fail "missing open paren in 'not' relation expression")
   *> relation_clause ~it_variable inner_terms
  <*
  if logically_positive then return ""
  else
    nonempty close_paren
    <|> fail "missing close paren in 'not' relation expression")
  >>= fun (_, e) ->
  if logically_positive then return (Clause e) else return (Not_expr (Clause e))

(* FIXME make a 'not' version of this using logically_positive *)
(*
    'examine ' 'the ___th  ___ of the ___: ' '<relation expression>'
*)
let examining_relation_expression inner_terms =
  (keyword "examining"
  <|> fail "no 'examining' in 'examining' relation expression")
  *> (choice inner_terms
     <|> fail "no examination argument in 'examining' relation expression")
  >>= fun it_val ->
  (nonempty colon <|> fail "no ':' in 'examining' relation expression")
  *> relation_clause ~it_variable:(Some it_val) inner_terms
  >>= fun (used_it_variable, Relation_clause (comp, left_term, right_expr)) ->
  if used_it_variable then
    return
      (Clause (ExaminingRelation_clause (comp, it_val, left_term, right_expr)))
  else
    fail
      ("did not use '# it' variable for the examination argument"
     ^ " in 'examining' relation expression")

(*
     ['___ with ___  ___' / '___ with a ___ of ___']
*)
let with_clause right_name =
  value_token >>= fun left_s ->
  (match_first_phrase (token_lists_of_phrases_by_length [ "with"; "with a" ])
  <|> fail "no 'with' keyword in 'with' clause")
  *> keyphrase right_name
  *> (keyword "of" <|> return "")
  *> (value_token
     <|> fail ("no <" ^ right_name ^ "> expression in 'with' clause"))
  >>= fun right_s -> return (With_term (right_name, left_s, right_s))

(*
     '___ of <size name> ___ <unit name>'
*)
let of_size_clause size_name unit_name =
  value_token >>= fun left_s ->
  (keyword "of" <|> fail "no 'of' keyword in 'of size' clause")
  *> keyphrase size_name
  *> (value_token
     <|> fail ("no <" ^ size_name ^ "> expression in 'of size' clause"))
  <* (keyphrase unit_name
     <|> fail ("no unit name '" ^ unit_name ^ "' in 'of size' clause"))
  >>= fun right_s ->
  return (OfSize_term (size_name, unit_name, left_s, right_s))

(*
    'is one of [___, ___, ____, ...]'
*)
let is_one_of_subexpression_pos_neg logically_positive value_terms =
  (nonempty open_bracket <|> fail "no opening bracket in 'is one of' expression")
  *> uniform_separated_expression
       true (* singleton allowed for better error message *)
       (choice value_terms)
       [ nonempty comma ]
       (fun _ vals -> TermList vals)
  >>= fun val_term_list ->
  (nonempty close_bracket
  <|> fail "no closing bracket in 'is one of' expression")
  *>
  match val_term_list with
  | TermList _ -> return (logically_positive, val_term_list)
  | _ -> fail "only 1 value in [] of 'is one of' expression"

let is_one_of_subexpression_only_pos value_terms =
  is_one_of_keyword_only_pos *> is_one_of_subexpression_pos_neg true value_terms
  >>= fun (_, val_term_list) -> return val_term_list

let is_one_of_subexpression value_terms =
  is_one_of_keyword_pos_neg >>= fun logically_positive ->
  is_one_of_subexpression_pos_neg logically_positive value_terms

(* FIXME make left_term_seq into a list of parsers instead of a parser *)
(*
     'the ___ ' ['is' / 'is that of' / ...] ' ___'
*)
let is_that_of_expression left_term_seq right_terms =
  ((keyword "a" <|> fail "no 'a' in 'is_that_of' expression") *> left_term_seq
   >>= function
   | [] -> fail "nothing in LHS of 'is that of' expression"
   | l_terms -> return l_terms)
  >>= fun l_terms ->
  (* UH OH: must attemp 'is one of' subexpression first
            because 'is' is a prefix of 'is one of' *)
  is_one_of_subexpression right_terms
  <|> ( is_keyword_pos_neg >>= fun logically_positive ->
        choice right_terms <|> fail "nothing in RHS of 'is that of' expression"
        >>= fun r_term -> return (logically_positive, r_term) )
  >>= fun (logically_positive, r_term) ->
  return
    (if logically_positive then Clause (IsThatOf (l_terms, r_term))
    else Not_expr (Clause (IsThatOf (l_terms, r_term))))

(*
     ' ___ that is ___'    (* ex. 'is ip4 that is broadcast' *)
*)
let that_is_expression left_term right_terms =
  left_term >>= fun l_term ->
  (match_first_phrase (token_lists_of_phrases_by_length [ "that" ])
  <|> fail "no 'that' keyword")
  *> is_keyword_pos_neg
  >>= fun logically_positive ->
  choice right_terms >>= fun r_term ->
  return
    (if logically_positive then Clause (ThatIs (l_term, r_term))
    else Not_expr (Clause (ThatIs (l_term, r_term))))

(* FIXME make middle_term_seq into a list of parsers instead of a parser *)
(*
     ' ___ that ' ['has ___  ___' / 'has a ___ of ___']
*)
let that_has_expression left_term middle_term_seq right_terms =
  left_term >>= fun l_term ->
  (match_first_phrase (token_lists_of_phrases_by_length [ "that" ])
  <|> fail "no 'that' keyword in 'that has' expression")
  *> has_keyword_pos_neg
  >>= fun logically_positive ->
  keyword "a" *> return true <|> return false >>= fun uses_a_keyword ->
  (middle_term_seq >>= function
   | [] -> fail "no phrases in MHS of 'that has' expression"
   | mid_terms -> return mid_terms)
  >>= fun mid_terms ->
  ((if uses_a_keyword then
    keyword "of" <|> fail "no 'of' in 'that has a ___ of ___' expression"
   else return "of")
   *> choice right_terms
  <|>
  if uses_a_keyword then
    (keyword "which"
    <|> fail "no 'which' in 'that has a ___ which is one of ___' expression")
    *> is_one_of_subexpression_only_pos right_terms
  else fail "no 'a' in 'that has a ___ which is one of ___' expression")
  >>= fun r_term ->
  if logically_positive then
    return (Clause (ThatHas (l_term, mid_terms, r_term)))
  else return (Not_expr (Clause (ThatHas (l_term, mid_terms, r_term))))

(*
     'of type ___'    (* ex. 'of type ip4' *)
*)
let of_type_expression right_terms =
  of_type_keyword_pos_neg >>= fun logically_positive ->
  choice right_terms >>= fun r_term ->
  return
    (if logically_positive then Clause (OfType r_term)
    else Not_expr (Clause (OfType r_term)))

let logically_negatable_nest_expression exprs nest_expr =
  let rec logically_positive_nest_expr =
    choice exprs <|> nest_expr <|> fail "unparseable"
  in
  choice exprs <|> nest_expr
  <|> ( keyword "not" *> nonempty open_paren *> logically_positive_nest_expr
      <* nonempty close_paren
      >>= fun e -> return (Not_expr e) )
  <|> fail "unparseable"

(*
   [  '___ ' ', and ____' ', and ____' ', and ____' ...
    / '___ ' ', or ____' ', or ____' ', or ____' ... ]
*)
let logical_junction_expression exprs nest_expr =
  uniform_separated_expression true
    (logically_negatable_nest_expression exprs nest_expr)
    [ nonempty comma *> keyword "and"; nonempty comma *> keyword "or" ]
    (fun sep es -> match sep with "and" -> And_expr es | "or" -> Or_expr es)

let if_and_only_if_expression exprs nest_expr =
  (* this uniform separated expression is necessary because
     otherwise there could be an infinite chain of 'if and only if's *)
  uniform_separated_expression false
    (logically_negatable_nest_expression exprs nest_expr)
    [ keyphrase "if and only if" ]
    (fun _ es ->
      match es with
      | [ expr_a; expr_b ] ->
          (* (A and B) or (not A and not B) *)
          Or_expr
            [
              And_expr [ expr_a; expr_b ];
              And_expr [ Not_expr expr_a; Not_expr expr_b ];
            ]
      | _ -> And_expr es)
  >>= fun e ->
  match e with
  | Or_expr _ -> return e
  | And_expr _ -> fail "too many 'if and only if' connectors"

(*
   'if ___ then ___ else ___'
*)
let if_then_else_expression exprs nest_expr =
  let inner_expr = logically_negatable_nest_expression exprs nest_expr in
  (keyword "if" <|> fail "no 'if' in 'if then (else)' expression")
  *> inner_expr (* <|> rest_of_expression_before (keyphrase "then") *)
  >>= fun expr_a ->
  (keyphrase "then" <|> fail "no 'then' in 'if then (else)' expression")
  *> inner_expr (* <|> rest_of_expression_before (keyword "else") *)
  >>= fun expr_b ->
  keyword "else" *> inner_expr (* <|> rest_of_expression *)
  >>= (fun expr_c ->
        (* ((not A) or B) and (A or C) *)
        return
          (And_expr
             [ Or_expr [ Not_expr expr_a; expr_b ]; Or_expr [ expr_a; expr_c ] ]))
  (* (not A) or B *)
  <|> return (Or_expr [ Not_expr expr_a; expr_b ])

(**
   recursively parses filter expression 
   until it reaches innermost nested expression
*)
let rec filter_expression atomic_exprs =
  (*
      WARN: boolean_expr <|> if_then_expr doesn't work because
           the rest_of_expression is inside the bool & if expressions

      technically it does work because
      if/then/else has a starting marker ("if")

      got rid of unparseable expressions (rest_of_expression),
      and it works now

      WARN: if_and_only_if_expr must be first because boolean_expr
           can take just 1 singleton expr
  *)
  if_and_only_if_expression atomic_exprs (nest_expression atomic_exprs)
  <|> logical_junction_expression atomic_exprs (nest_expression atomic_exprs)
  <|> if_then_else_expression atomic_exprs (nest_expression atomic_exprs)

and nest_expression atomic_exprs =
  open_paren >>= fun open_p ->
  (match open_p with
  | "(" -> return "("
  | _ -> fail "not the start of nested expression")
  *> filter_expression atomic_exprs
  >>= fun expr ->
  close_paren >>= fun close_p ->
  match close_p with ")" -> return expr | _ -> fail "unmatched opening paren?"

(** 
    parses a nestable english expression 
    given a list of atomic expressions
*)
let english_expression atomic_exprs =
  filter_expression atomic_exprs >>= fun expr ->
  (* after this is anything un-parseable *)
  rest_of_expression >>= function
  | Clause (Unit (UnidentifiedString "")) -> return expr
  | Clause (Unit (UnidentifiedString rest)) ->
      fail ("unparseable English: '" ^ rest ^ "'")
