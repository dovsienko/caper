# Examples of using Caper
Caper is a tool for converting packet filter expressions between different types of representations.

## Expanding pcap expressions "in full"

Caper can be used to turn the implicit meaning in pcap expressions into explicit syntax.

```
$ ./caper.byte -q -p -e "dst port http"

    ether proto \ip
    &&
    (ip proto \sctp && sctp dst port 80
      || ip proto \tcp && tcp dst port 80
      || ip proto \udp && udp dst port 80)
  ||
    ether proto \ip6
    &&
    (ip6 proto \sctp && sctp dst port 80
      || ip6 proto \tcp && tcp dst port 80
      || ip6 proto \udp && udp dst port 80)
```

```
$ ./caper.byte -q -p -e "src foo"
ether proto \arp && arp src host foo
  || ether proto \ip && ip src host foo
  || ether proto \ip6 && ip6 src host foo
  || ether proto \rarp && rarp src host foo
```

## Compiling pcap expressions to BPF

Caper's BPF compiler is compatible with that of libpcap/tcpdump, and offers some improvements.
The compiler is invoked using the `-BPF` flag:

```
$ ./caper.byte -BPF -q -p -e "ip src host 192.168.0.1 or ip src host 192.168.0.2"
(000) ldh [12]
(001) jeq #0x800               jt 2      jf 4
(002) ld [26]
(003) jeq #0xc0a80001          jt 8      jf 4
(004) ldh [12]
(005) jeq #0x800               jt 6      jf 9
(006) ld [26]
(007) jeq #0xc0a80002          jt 8      jf 9
(008) ret #262144
(009) ret #0
```

To produce optimized code, use the `-BPF_optimized` flag. For the previous example, the optimizer produces the following (shorter) code:

```
./caper.byte -BPF_optimized -q -p -e "ip src host 192.168.0.1 or ip src host 192.168.0.2"
(000) ldh [12]
(001) jeq #0x800               jt 2      jf 6
(002) ld [26]
(003) jeq #0xc0a80001          jt 5      jf 4
(004) jeq #0xc0a80002          jt 5      jf 6
(005) ret #262144
(006) ret #0
```

## BPF compilation improvements over libpcap/tcpdump

### IPv6

There are cases involving IPv6 that are not currently handled by libpcap/tcpdump, such as:

```
$ tcpdump -d "ip6 and tcp[tcpflags]=tcp-ack"
tcpdump: expression rejects all packets
```

Caper can handle several of these cases, for example:

```
$ ./caper.byte -BPF_optimized -q -p -e "ip6 and tcp[tcpflags]=tcp-ack"
(000) ldh [12]
(001) jeq #0x86dd jt 2 jf 7
(002) ldb [20]
(003) jeq #0x6 jt 4 jf 7
(004) ldb [67]
(005) jeq #0x10 jt 6 jf 7
(006) ret #262144
(007) ret #0
```

### "protochain"

Caper's BPF compiler accepts the `-max_rec` parameter to set the maximum extent
of the nested encapsulation (pcap's "protochain" syntax) to filter:

```
$./caper.byte -not_expand -BPF_optimized -max_rec 2 -q -p -e "ether proto \ip6 && ip6 protochain 58 && icmp6 protochain 17"
(000) ldh [12]
(001) jeq #0x86dd              jt 2      jf 192
...(omitted for brevity)
(187) add x
(188) tax
(189) ld M[15]
(190) jeq #0x11                jt 191    jf 192
(191) ret #262144
(192) ret #0
```

### VLAN range

VLAN range checks are handled specially by the compiler, as shown below:

```
$./caper.byte -not_expand -BPF_optimized -q -p -e "vlan 2000-3000"
(000) ldh [12]
(001) jeq #0x8100 jt 4 jf 2
(002) jeq #0x88a8 jt 4 jf 3
(003) jeq #0x9100 jt 4 jf 9
(004) ldh [14]
(005) and #0xfff
(006) jge #0x7d0 jt 7 jf 9
(007) jgt #0xbb8 jt 9 jf 8
(008) ret #262144
(009) ret #0
```

## Converting between pcap expressions and English

Use `-engl-in` to use English input and produce a pcap expression:

```
$ ./caper.byte -engl-in -q -e "IPv4 that has a host of 192.168.0.2"
ip host 192.168.0.2
```

Use `-engl-out` to convert a pcap expression into English:

```
$ ./caper.byte -engl-out -q -e "tcp port 80 or 443"
tcp that has a port which is one of [80, 443]
```
