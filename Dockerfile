FROM debian:bullseye-slim

RUN apt-get update \
    && apt-get install -y m4 \
    && apt-get install -y opam
RUN git clone https://gitlab.com/niksu/caper
RUN opam init -y --shell-setup --disable-sandboxing
RUN . ~/.profile \
    && opam install -y ocamlfind \
    && opam install -y ocamlbuild \
    && opam install -y menhir
RUN . ~/.profile \
    && cd caper \
    && ./build.sh caper.native

ENTRYPOINT ["/caper/caper.native"]
