#!/bin/bash
#
# Test consists of checking that the exit status is 0.
# Nik Sultana, April 2023.
#
# NOTE output from Caper is sent to /dev/null

CAPER="$1"
QUERY="$2"
TEST_ID="$3"

# FIXME const
TIMEOUT=5s

if [[ "${CAPER}" = "" || "${QUERY}" = "" ]]
then
  echo "Need to specify path to Caper, and a query" >&2
  exit 1
fi

echo "${QUERY}" | timeout -v ${TIMEOUT} ${CAPER} 2>&1 > /dev/null
RESULT=$?

[[ 0 = "${RESULT}" ]] && echo "${TEST_ID}: OK : ${QUERY}" || echo "${TEST_ID}: FAIL : '${QUERY}'"
