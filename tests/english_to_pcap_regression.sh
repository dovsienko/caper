#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for English to Pcap
# Marelle Leon, March 2023

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=$(basename "$0")

function tst() {
  if [ "$#" -ne 3 ]; then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$3" "$1"
}


tst "E1" "not (a source is foo)" "! src foo"

tst "E2" "not (a source is foo), and a source is foo" "(! src foo) && src foo"

tst "E3" "a source is foo, and not (a source is foo)" "src foo && (! src foo)"

tst "E4" "a source is foo" "src foo"

tst "E5" "a source is not foo" "! src foo"

tst "E6" "a destination network is 10" "dst net 10"

tst "E7" "a destination network is not 10
" "! dst net 10"

tst "E8" "a source host is host" "src host \\host"

tst "E9" "a source host is not host
" "! src host \\host"

tst "E10" "a port range is 6000-6008
" "portrange 6000-6008"

tst "E11" "a port range is not 6000-6008
" "! portrange 6000-6008"

tst "E12" "of type ip4" "ip"

tst "E13" "not of type tcp" "! tcp"

tst "E14" "ip4 that has source foo" "ip src foo"

tst "E15" "ip4 that does not have source foo
" "! ip src foo"

tst "E16" "ip4 that has a source of foo" "ip src foo"

tst "E17" "ip4 that does not have a source of foo
" "! ip src foo"

tst "E18" "ip4 that is broadcast" "ip broadcast"

tst "E19" "ip4 that is not broadcast" "! ip broadcast"

tst "E20" "a destination network is 172.16 with mask 255.255.0.0
" "dst net 172.16 mask 255.255.0.0"

tst "E21" "a destination network is 172.16 with a mask of 255.255.0.0
" "dst net 172.16 mask 255.255.0.0"

tst "E22" "a destination network is not 172.16 with mask 255.255.0.0
" "! dst net 172.16 mask 255.255.0.0"

tst "E23" "a destination network is 172.16 of length 16 bits
" "dst net 172.16/16"

tst "E24" "a destination network is 10 of length 8 bits
" "dst net 10/8"

tst "E25" "
        ip4 that has a source of foo, or
        arp that has a source of foo, or
        rarp that has a source of foo
" "ip src foo || arp src foo || rarp src foo"

tst "E26" "
        ip4 that has a network of bar, or
        arp that has a network of bar, or
        rarp that has a network of bar
" "ip net bar || arp net bar || rarp net bar"

tst "E27" "
        tcp that has a port of 53, or
        udp that has a port of 53
" "tcp port 53 || udp port 53"

tst "E28" "
        ip4 that has a network of bar, or
        arp that has a network of bar, or
        rarp that has a network of bar
" "ip net bar || arp net bar || rarp net bar"

tst "E29" "
        a host is foo, and 
        a port is not ftp, and 
        a port is not ftp-data
" "host foo && (! port ftp) && (! port ftp-data)"

tst "E30" "
        tcp that has a destination port of ftp, or
        tcp that has a destination port of ftp-data, or
        tcp that has a destination port of domain
" "tcp dst port ftp || tcp dst port ftp-data || tcp dst port domain"

tst "E31" "
        ethernet that has a proto of ip4, and
        a host is host
" "ether proto \ip && host \host"

tst "E32" "ethernet that has a destination of ehost
" "ether dst ehost"

tst "E33" "ethernet that has a destination of ehost
" "ether dst ehost"

tst "E34" "ethernet that has a destination of ehost
" "ether dst ehost"

tst "E35" "ethernet that has a source of ehost
" "ether src ehost"

tst "E36" "ethernet that has a host of ehost
" "ether host ehost"

tst "E37" "ethernet that has a destination of ehost
" "ether dst ehost"

tst "E38" "a gateway is host" "gateway \host"

tst "E39" "
        ethernet that has a host of ehost, and
        a host is not host
" "ether host ehost && (! host \host)"

tst "E40" "a host is not vs, and a host is not ace
" "(! host vs) && (! host ace)"

tst "E41" "a host is not vs, and a host is ace
" "(! host vs) && host ace"

tst "E42" "{0} equals {0}" "0 = 0"

tst "E43" "{ip[0]} equals {0}
" "ip[0] = 0"

tst "E44" "not ({ip[0]} equals {0})
" "! ip[0] = 0"

tst "E45" "
        {# the 0th byte of the ethernet header} 
        does not equal {0}
" "ether[0] != 0"

tst "E46" "
        {# the 0th byte of the ethernet header & 1} 
        does not equal {0}
" "(ether[0] & 1) != 0"

tst "E47" "
        {# the 0th byte of the ip4 header & 0xf} 
        does not equal {5}
" "(ip[0] & 0xf) != 5"

tst "E48" "
        {# the 6th 2 bytes of the ip4 header & 0x1fff} 
        equals {0}
" "(ip[6 : 2] & 0x1fff) = 0"

tst "E49" "
        {tcp[tcpflags] & (tcp-syn | tcp-fin)} does not equal {0}
" "(tcp[tcpflags] & (tcp-syn | tcp-fin)) != 0"

tst "E50" "
        examining the 1st byte of the tcp header:  
        it equals {25}
" "tcp[1] = 25"

tst "E51" "
        examining the 0th byte of the ethernet header:
        {1 & # it} does not equal {0}
" "(1 & ether[0]) != 0"

tst "E52" "
        examining the 0th byte of the ip4 header:
        {# it & 0xf} does not equal {5}
" "(ip[0] & 0xf) != 5"

tst "E53" "
        examining the 6th 2 bytes of the ip4 header: 
        {# it & 0x1fff} equals {0}
" "(ip[6 : 2] & 0x1fff) = 0"

tst "E54" "if a source is foo then a host is ace
" "(! src foo) || host ace"

tst "E55" "
        if not (a source is foo) then not (a host is ace)
" "(! (! src foo)) || (! host ace)"

tst "E56" "if a port is ftp then a net is 10
" "(! port ftp) || net 10"

tst "E57" "
        if a source is foo 
        then (if a port is ftp 
              then a net is 10)
" "(! src foo) || ((! port ftp) || net 10)"

tst "E58" "
        if (a source is foo, and a net is 42) 
        then a host is ace
" "(! (src foo && net 42)) || host ace"

tst "E59" "
        if a source is foo 
        then a host is ace
        else ip4 that has destination foo
" "((! src foo) || host ace) && (src foo || ip dst foo)"

tst "E60" "
        if tcp that has source port 443
        then the 5th byte of the tcp header is {5} 
        else the 4th byte of the tcp header is less than {5}
" "((! tcp src port 443) || tcp[5] = 5) && (tcp src port 443 || tcp[4] < 5)"

tst "E61" "a port is one of [ftp, ftp-data]
" "port ftp || ftp-data"

tst "E62" "a port is in [ftp, ftp-data]
" "port ftp || ftp-data"

tst "E63" "a port is not one of [ftp, ftp-data]
" "! (port ftp || ftp-data)"

tst "E64" "a port is not in [ftp, ftp-data]
" "! (port ftp || ftp-data)"

tst "E65" "a port is one of [ftp, ftp-data, domain]
" "port ftp || ftp-data || domain"

tst "E66" "
        tcp that has a destination port 
        which is one of [ftp, ftp-data, domain]
" "tcp dst port ftp || ftp-data || domain"

tst "E67" "
        tcp that has a destination port 
        which is in [ftp, ftp-data, domain]
" "tcp dst port ftp || ftp-data || domain"

tst "E68" "
        a port is ftp if and only if a source is foo
" "(port ftp && src foo) || ((! port ftp) && (! src foo))"

tst "E69" "
        not (a port is ftp) if and only if not (a source is foo)
" "((! port ftp) && (! src foo)) || ((! (! port ftp)) && (! (! src foo)))"

tst "E70" "
        (a port is ftp if and only if a source is foo), and a net is 10
" "((port ftp && src foo) || ((! port ftp) && (! src foo))) && net 10"

tst "E71" "
        a port is ftp if and only if (a source is foo, and a net is 10)
" "(port ftp && (src foo && net 10)) || ((! port ftp) && (! (src foo && net 10)))"

tst "E72" "
        (a port is ftp, and a net is 10) if and only if a source is foo
" "((port ftp && net 10) && src foo) || ((! (port ftp && net 10)) && (! src foo))"

tst "E73" "
        a port is ftp, and (a net is 10 if and only if a source is foo)
" "port ftp && ((net 10 && src foo) || ((! net 10) && (! src foo)))"

tst "E74" "
        ethernet that is broadcast
" "ether broadcast"

tst "E75" "
        ethernet that has a proto of IPv4
" "ether proto \ip"

tst "E76" "
        ethernet that is broadcast
" "ether broadcast"
