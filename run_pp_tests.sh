#!/bin/bash
# Caper: a pcap expression analysis utility.
# Run regression tests wrt pretty printer.
# Nik Sultana, February 2023

# FIXME sensitive to the directory in which this is run
tests/pp_regression.sh "./caper.byte -1stdin -q -p" tests/disambiguate_test.sh
