(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: AST spec for pcap expressions
*)


type typ =
  | Host
  | Net
  | Port_type
  | Portrange
  | Proto (*NOTE proto and protochain can be symbolic or numeric*)
  | Protochain
  | Gateway
  | Less
  | Greater

type dir =
  | Src
  | Dst
  | Src_or_dst
  | Src_and_dst
  | Inbound
  | Outbound
  | Broadcast
  | Multicast

let simple_dir (dir : dir) : bool =
  match dir with
  | Src
  | Dst
  | Src_or_dst
  | Src_and_dst -> true
  | _ -> false

type proto =
  | Ether
(*
  | Fddi
  | Tr
  | Wlan
  | Decnet
*)
  | Ip
  | Ip6
  | Arp
  | Rarp
  | Tcp
  | Udp
  | Sctp
  | Vlan
  | Mpls
  | Icmp
  | Icmp6

type mask_size = int
type mask = string
type value_atom =
  | Nothing
  | String of string
  | Escaped_String of string
  | IPv4_Address of int * int * int * int
  | IPv6_Address of string(*display address*) * string(*fully-expanded address*)
  | IPv4_Network of int * int option * int option * int option * mask_size option
  | IPv4_Network_Masked of int * int option * int option * int option * mask
  | IPv6_Network of string(*display address*) * string(*fully-expanded address*) * string(*prefix length*)
  | MAC_Address of string
  | Port of string (*since may be symbolic value, such as "ftp-data"*)
  | Port_Range of string * string
(*FIXME at parse time check the ranges of ports, port_range, ipv4 addresses,
        and the length of netmasks*)
(*  | And of value list -- NOTE there doesn't seem to be any need for conjunction at this level, since it's either redundant or leads to contradictions*)
(*  | Or of value list -- NOTE it doesn't make sense to have a "polytypic" disjunction*)

(*
  | Or_String of string list

  | Or_Escaped_String of string list
  | Or_IPv4_Address of (int * int * int * int) list

  | Or_IPv6_Address of string list
  | Or_IPv6_Network of (string * string) list
  | Or_IPv4_Network of (int * int option * int option * int option * mask_size option) list
  | Or_IPv4_Network_Masked of (int * int option * int option * int option * mask) list
  | Or_MAC_Address of string list

  | Or_Port of string list
  | Or_Port_Range of (string * string) list
*)

type field = proto option * dir option * typ option
type primitive = field * value_atom

(*The most uninformative primitive -- matches nothing, but doesn't denote false*)
let empty = ((None, None, None), Nothing)

type no_bytes =
  | One
  | Two
  | Four

type expr =
  | Identifier of string
  | Nat_Literal of int
  | Hex_Literal of string
  | Len
  | Plus of expr list
  | Times of expr list
  | Binary_And of expr list
  | Binary_Or of expr list
  | Binary_Xor of expr list
  | Minus of expr * expr
  | Quotient of expr * expr
  | Mod of expr * expr
  | Packet_Access of string(*NOTE should be some protocol -- this is checked at parse time*) * expr * no_bytes option
  | Shift_Right of expr * expr
  | Shift_Left of expr * expr

type rel_expr =
  | Gt of expr * expr
  | Lt of expr * expr
  | Geq of expr * expr
  | Leq of expr * expr
  | Eq of expr * expr
  | Neq of expr * expr

type pcap_expression =
  | Primitive of primitive
  | Atom of rel_expr
  | And of pcap_expression list
  | Or of pcap_expression list
  | Not of pcap_expression
  | True
  | False
