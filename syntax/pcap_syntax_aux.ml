(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Supporting functions for Pcap syntax processing
*)

open Pcap_syntax
open String_features


let string_of_no_bytes = function
  | One -> "1"
  | Two -> "2"
  | Four -> "4"

let rec size_of_expr : expr -> int = function
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len
  | Shift_Right (_, _) (*FIXME take into account size of subexpressions?*)
  | Shift_Left (_, _) (*FIXME take into account size of subexpressions?*)
  | Packet_Access (_, _, _) (*FIXME take into account size of subexpressions?*)
    -> 1
  | Plus es
  | Times es
  | Binary_And es
  | Binary_Or es
  | Binary_Xor es -> List.fold_right (fun e acc -> acc + size_of_expr e) es 1
  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Mod (e1, e2) -> 1 + size_of_expr e1 + size_of_expr e2

let rec sized_enbracket e =
  let e_s = string_of_expr e in
  if size_of_expr e > 1 then "(" ^ e_s ^ ")" else e_s

and string_of_expr = function
  | Identifier s -> s
  | Nat_Literal i -> string_of_int i
  | Hex_Literal v -> v
  | Len -> "len"
  | Plus es ->
    begin
      match es with
      | [] -> failwith "Empty addition"
      | [pe] ->
          if !Config.relaxed_mode then
            sized_enbracket pe
          else failwith "Singleton addition"
      | _ ->
          let result =
            List.map sized_enbracket es
            |> String.concat " + " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Minus (e1, e2) ->
    sized_enbracket e1 ^ " - " ^ sized_enbracket e2
  | Times es ->
    List.map sized_enbracket es
    |> String.concat " * "
  | Quotient (e1, e2) ->
    sized_enbracket e1 ^ " / " ^ sized_enbracket e2
  | Mod (e1, e2) ->
    sized_enbracket e1 ^ " % " ^ sized_enbracket e2
  | Binary_And es ->
    begin
      match es with
      | [] -> failwith "Empty binary AND"
      | [pe] ->
          if !Config.relaxed_mode then
            sized_enbracket pe
          else failwith "Singleton binary AND"
      | _ ->
          let result =
            List.map sized_enbracket es
            |> String.concat " & " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Binary_Or es ->
    begin
      match es with
      | [] -> failwith "Empty binary OR"
      | [pe] ->
          if !Config.relaxed_mode then
            sized_enbracket pe
          else failwith "Singleton binary OR"
      | _ ->
          let result =
            List.map sized_enbracket es
            |> String.concat " | " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Binary_Xor es ->
    begin
      match es with
      | [] -> failwith "Empty binary XOR"
      | [pe] ->
          if !Config.relaxed_mode then
            sized_enbracket pe
          else failwith "Singleton binary XOR"
      | _ ->
          let result =
            List.map sized_enbracket es
            |> String.concat " ^ " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Shift_Right (e1, e2) ->
    sized_enbracket e1 ^ " >> " ^ sized_enbracket e2
  | Shift_Left (e1, e2) ->
    sized_enbracket e1 ^ " << " ^ sized_enbracket e2
  | Packet_Access (s, e, nb_opt) ->
    let nb_s =
      match nb_opt with
      | None -> ""
      | Some nb -> " : " ^ string_of_no_bytes nb in
    s ^ "[" ^ string_of_expr e ^ nb_s ^ "]"

let string_of_rel_expr = function
  | Gt (e1, e2) -> sized_enbracket e1 ^ " > " ^ sized_enbracket e2
  | Lt (e1, e2) -> sized_enbracket e1 ^ " < " ^ sized_enbracket e2
  | Geq (e1, e2) -> sized_enbracket e1 ^ " >= " ^ sized_enbracket e2
  | Leq (e1, e2) -> sized_enbracket e1 ^ " <= " ^ sized_enbracket e2
  | Eq (e1, e2) -> sized_enbracket e1 ^ " = " ^ sized_enbracket e2
  | Neq (e1, e2) -> sized_enbracket e1 ^ " != " ^ sized_enbracket e2

let string_of_typ = function
  | Host -> "host"
  | Net -> "net"
  | Port_type -> "port"
  | Portrange -> "portrange"
  | Proto -> "proto"
  | Protochain -> "protochain"
  | Gateway -> "gateway"
  | Less -> "less"
  | Greater -> "greater"

let string_of_dir = function
  | Src -> "src"
  | Dst -> "dst"
  | Src_or_dst -> "src or dst"
  | Src_and_dst -> "src and dst"
  | Inbound -> "inbound"
  | Outbound -> "outbound"
  | Broadcast -> "broadcast"
  | Multicast -> "multicast"

let string_of_proto = function
  | Ether -> "ether"
(*
  | Fddi -> "fddi"
  | Tr -> "tr"
  | Wlan -> "wlan"
  | Decnet -> "decnet"
*)
  | Ip -> "ip"
  | Ip6 -> "ip6"
  | Arp -> "arp"
  | Rarp -> "rarp"
  | Tcp -> "tcp"
  | Udp -> "udp"
  | Sctp -> "sctp"
  | Vlan -> "vlan"
  | Mpls -> "mpls"
  | Icmp -> "icmp"
  | Icmp6 -> "icmp6"

let proto_of_string = function
  | "ether" -> Ether
(*
  | "fddi" -> Fddi
  | "tr" -> Tr
  | "wlan" -> Wlan
  | "decnet" -> Decnet
*)
  | "ip" -> Ip
  | "ip6" -> Ip6
  | "arp" -> Arp
  | "rarp" -> Rarp
  | "tcp" -> Tcp
  | "udp" -> Udp
  | "sctp" -> Sctp
  | "vlan" -> Vlan
  | "mpls" -> Mpls
  | "icmp" -> Icmp
  | "icmp6" -> Icmp6
  | s -> failwith ("proto_of_string: unrecognised protocol: " ^ s)

let string_of_id_atom
 ?debug_output:(debug_output = false)
 (va : value_atom) : string =
  let str_of_escaped_string s = "\\" ^ s in
  let str_of_ipv4_address (i1, i2, i3, i4) =
    string_of_int i1 ^ "." ^
    string_of_int i2 ^ "." ^
    string_of_int i3 ^ "." ^
    string_of_int i4 in
  let str_of_ipv6_address address = address in
  let str_of_ipv4_network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt) =
    let i2s, i3s, i4s, mask_size_s =
      match i2_opt, i3_opt, i4_opt, mask_size_opt with
      | None, None, None, None ->
        "", "", "", ""
      | None, None, None, Some mask_size ->
        "", "", "", "/" ^ string_of_int mask_size
      | Some i2, None, None, None ->
        "." ^ string_of_int i2, "", "", ""
      | Some i2, None, None, Some mask_size ->
        "." ^ string_of_int i2, "", "", "/" ^ string_of_int mask_size
      | Some i2, Some i3, None, None ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, "", ""
      | Some i2, Some i3, None, Some mask_size ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, "", "/" ^ string_of_int mask_size
      | Some i2, Some i3, Some i4, None ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, "." ^ string_of_int i4, ""
      | Some i2, Some i3, Some i4, Some mask_size ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, "." ^ string_of_int i4, "/" ^ string_of_int mask_size
      | _, _, _, _ -> failwith "Unexpected info"(*FIXME give more info*) in
    string_of_int i1 ^ i2s ^ i3s ^ i4s ^ mask_size_s in
  let str_of_ipv4_network_masked (i1, i2_opt, i3_opt, i4_opt, mask) =
    let i2s, i3s, i4s =
      match i2_opt, i3_opt, i4_opt with
      | None, None, None ->
        "", "", ""
      | Some i2, None, None ->
        "." ^ string_of_int i2, "", ""
      | Some i2, Some i3, None ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, ""
      | Some i2, Some i3, Some i4 ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, "." ^ string_of_int i4
      | _, _, _ -> failwith "Unexpected info"(*FIXME give more info*) in
    string_of_int i1 ^ i2s ^ i3s ^ i4s ^ " mask " ^ mask in
  let str_of_ipv6_network (address, mask_length) =
    address ^ "/" ^ mask_length in
  let str_of_mac_address address = address in
  let str_of_port_range (from, until) = from ^ "-" ^ until in
  match va with
  | Nothing ->
      if debug_output then "Nothing" else ""
  | String s ->
      if debug_output then "String " ^ s else s
  | Escaped_String s ->
      let result = str_of_escaped_string s in
      if debug_output then "Escaped_String " ^ result else result
  | IPv4_Address (i1, i2, i3, i4) ->
      let result = str_of_ipv4_address (i1, i2, i3, i4) in
      if debug_output then "IPv4_Address " ^ result else result
  | IPv6_Address (orig_address, expanded_address) ->
      let result =  str_of_ipv6_address orig_address in
      if debug_output then "IPv6_Address " ^ result else result
  | IPv4_Network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt) ->
      let result = str_of_ipv4_network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt) in
      if debug_output then "IPv4_Network " ^ result else result
  | IPv4_Network_Masked (i1, i2_opt, i3_opt, i4_opt, mask) ->
      let result = str_of_ipv4_network_masked (i1, i2_opt, i3_opt, i4_opt, mask) in
      if debug_output then "IPv4_Network_Masked " ^ result else result
  | IPv6_Network (orig_address, expanded_addres, mask_length) ->
      let result = str_of_ipv6_network (orig_address, mask_length) in
      if debug_output then "IPv6_Network " ^ result else result
  | MAC_Address address ->
      let result = str_of_mac_address address in
      if debug_output then "MAC_Address " ^ result else result
  | Port i ->
      let result = i in
      if debug_output then "Port " ^ result else result
  | Port_Range (from, until) ->
      let result = str_of_port_range (from, until) in
      if debug_output then "Port_Range " ^ result else result

let string_of_primitive (((proto_opt, dir_opt, typs_opt), id_atom) : primitive) : string =
  let proto_s =
    match proto_opt with
    | None -> ""
    | Some proto -> string_of_proto proto in
  let dir_s =
    match dir_opt with
    | None -> ""
    | Some dir ->
      (if proto_s <> "" then " " else "") ^ string_of_dir dir in
  let typs_s =
    match typs_opt with
    | None -> ""
    | Some ty ->
      (if proto_s <> "" || dir_s <> "" then " " else "") ^ string_of_typ ty in
  let id_atom_s =
    match string_of_id_atom id_atom with
    | "" -> ""
    | s -> (if proto_s <> "" || dir_s <> "" || typs_s <> "" then " " else "") ^ s
  in proto_s ^ dir_s ^ typs_s ^ id_atom_s

let rec size_of_pcap_expression : pcap_expression -> int = function
  | Primitive _
  | Atom _
  | True
  | False -> 1
  | And pes
  | Or pes ->
    List.fold_right (fun pe acc ->
      acc + size_of_pcap_expression pe) pes 1
  | Not pe -> 1 + size_of_pcap_expression pe

let rec sized_enbracket_pe ?(relaxed : bool = false) ?(stringifier : (pcap_expression -> string) option = None) pe =
  let pe_s =
   match stringifier with
   | None -> string_of_pcap_expression ~relaxed pe
   | Some f -> f pe in
  if size_of_pcap_expression pe > 1 then "(" ^ pe_s ^ ")" else pe_s

and string_of_pcap_expression ?(relaxed : bool = false) (pe : pcap_expression) : string =
  match pe with
  | Primitive prim -> string_of_primitive prim
  | Atom re -> string_of_rel_expr re
  | And pes ->
    begin
      match pes with
      | [] ->
          if relaxed || !Config.relaxed_mode then
            "(empty and)"
          else failwith "Empty conjunct"
      | [pe] ->
          if relaxed || !Config.relaxed_mode then
            sized_enbracket_pe ~relaxed pe
          else failwith ("Singleton conjunct: " ^ string_of_pcap_expression pe ^ "\n")
      | _ ->
          let result =
            List.map (sized_enbracket_pe ~relaxed) pes
            |> String.concat " && " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Or pes ->
    begin
      match pes with
      | [] ->
          if relaxed || !Config.relaxed_mode then
            "(empty or)"
          else failwith "Empty disjunct"
      | [pe] ->
          if relaxed || !Config.relaxed_mode then
            sized_enbracket_pe ~relaxed pe
          else failwith ("Singleton disjunct: " ^ string_of_pcap_expression pe ^ "\n")
      | _ ->
          let result =
            List.map (sized_enbracket_pe ~relaxed) pes
            |> String.concat " || " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Not pe -> "! " ^ sized_enbracket_pe ~relaxed pe
  | True -> "@: True :@"
  | False -> "@: False :@"


let id_K x = x

let primitive_f proto_f dir_f typs_f
 (((proto_opt, dir_opt, typs_opt), va) : primitive) : primitive =
 ((proto_f proto_opt, dir_f dir_opt, typs_f typs_opt), va)

let set_dir dir pid =
  let dir_f dir_opt =
    match dir_opt with
    | None ->
      Some dir
    | Some _ ->
      failwith "'dir' should not be set"(*FIXME give more info*) in
  let _ =
    (*Check to ensure that primitive isn't of an illegal form*)
    match pid with
    | ((_, _, Some typ), _) ->
      begin
        match typ with
        | Proto
        | Protochain
        | Gateway
        | Less
        | Greater -> failwith ("Cannot apply direction to the type '" ^ string_of_typ typ ^ "'")
        | _ -> ()
      end
    | _ -> ()
  in primitive_f id_K dir_f id_K pid

let set_proto proto pid =
  let proto_f proto_opt =
    match proto_opt with
    | None -> Some proto
    | Some _ ->
        failwith ("Cannot set more than one protocol")(*FIXME give more info*)
  in primitive_f proto_f id_K id_K pid

let set_typ typ pid =
  let typ_f typ_opt =
    match typ_opt with
    | None -> Some typ
    | Some _ ->
        failwith ("Cannot set more than one type")(*FIXME give more info*)
  in primitive_f id_K id_K typ_f pid

let set_value v pid =
  match pid with
  | (field, Nothing) -> (field, v)
  | _ -> failwith ("Cannot set more than one value")(*FIXME give more info*)

let free_vars_in_va (va : value_atom) : string list =
  match va with
  | String x
  | Port x ->
      if is_var_name x then [x] else []
(*
  | Or_String xs
  | Or_Port xs -> List.filter is_var_name xs
*)

  | Nothing
  | Escaped_String _
  | IPv4_Address _
  | IPv6_Address _
  | IPv4_Network _
  | IPv4_Network_Masked _
  | IPv6_Network _
  | MAC_Address _
  | Port_Range _ -> []
(*
  | Or_Escaped_String _
  | Or_IPv4_Address _
  | Or_IPv6_Address _
  | Or_IPv6_Network _
  | Or_IPv4_Network _
  | Or_IPv4_Network_Masked _
  | Or_MAC_Address _
  | Or_Port_Range _ -> []
*)
let free_vars_in_primitive ((_, va) : primitive) : string list =
  free_vars_in_va va

let is_primitive : pcap_expression -> bool = function
  | Primitive _ -> true
  | _ -> false

 let rec flatten_expr_plus (e : expr) : expr list =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> [e]

  | Plus es ->
      List.map flatten_expr_plus es
      |> List.concat

  | Times es -> [Times (List.map flatten_expr es)]
  | Binary_And es -> [Binary_And (List.map flatten_expr es)]
  | Binary_Or es -> [Binary_Or (List.map flatten_expr es)]
  | Binary_Xor es -> [Binary_Xor (List.map flatten_expr es)]

  | Minus (e1, e2) ->
      [Minus (flatten_expr e1, flatten_expr e2)]
  | Quotient (e1, e2) ->
      [Quotient (flatten_expr e1, flatten_expr e2)]
  | Mod (e1, e2) ->
      [Mod (flatten_expr e1, flatten_expr e2)]
  | Shift_Right (e1, e2) ->
      [Shift_Right (flatten_expr e1, flatten_expr e2)]
  | Shift_Left (e1, e2) ->
      [Shift_Left (flatten_expr e1, flatten_expr e2)]

  | Packet_Access (s, e, nb_opt) ->
      [Packet_Access (s, flatten_expr e, nb_opt)]

and flatten_expr_times (e : expr) : expr list =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> [e]

  | Plus es -> [Plus (List.map flatten_expr es)]
  | Times es ->
      List.map flatten_expr_times es
      |> List.concat
  | Binary_And es -> [Binary_And (List.map flatten_expr es)]
  | Binary_Or es -> [Binary_Or (List.map flatten_expr es)]
  | Binary_Xor es -> [Binary_Xor (List.map flatten_expr es)]

  | Minus (e1, e2) ->
      [Minus (flatten_expr e1, flatten_expr e2)]
  | Quotient (e1, e2) ->
      [Quotient (flatten_expr e1, flatten_expr e2)]
  | Mod (e1, e2) ->
      [Mod (flatten_expr e1, flatten_expr e2)]
  | Shift_Right (e1, e2) ->
      [Shift_Right (flatten_expr e1, flatten_expr e2)]
  | Shift_Left (e1, e2) ->
      [Shift_Left (flatten_expr e1, flatten_expr e2)]

  | Packet_Access (s, e, nb_opt) ->
      [Packet_Access (s, flatten_expr e, nb_opt)]

and flatten_expr_band (e : expr) : expr list =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> [e]

  | Plus es -> [Plus (List.map flatten_expr es)]
  | Times es -> [Times (List.map flatten_expr es)]
  | Binary_And es ->
      List.map flatten_expr_band es
      |> List.concat
  | Binary_Or es -> [Binary_Or (List.map flatten_expr es)]
  | Binary_Xor es -> [Binary_Xor (List.map flatten_expr es)]

  | Minus (e1, e2) ->
      [Minus (flatten_expr e1, flatten_expr e2)]
  | Quotient (e1, e2) ->
      [Quotient (flatten_expr e1, flatten_expr e2)]
  | Mod (e1, e2) ->
      [Mod (flatten_expr e1, flatten_expr e2)]
  | Shift_Right (e1, e2) ->
      [Shift_Right (flatten_expr e1, flatten_expr e2)]
  | Shift_Left (e1, e2) ->
      [Shift_Left (flatten_expr e1, flatten_expr e2)]

  | Packet_Access (s, e, nb_opt) ->
      [Packet_Access (s, flatten_expr e, nb_opt)]

and flatten_expr_bor (e : expr) : expr list =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> [e]

  | Plus es -> [Plus (List.map flatten_expr es)]
  | Times es -> [Times (List.map flatten_expr es)]
  | Binary_And es -> [Binary_And (List.map flatten_expr es)]
  | Binary_Or es ->
      List.map flatten_expr_bor es
      |> List.concat
  | Binary_Xor es -> [Binary_Xor (List.map flatten_expr es)]

  | Minus (e1, e2) ->
      [Minus (flatten_expr e1, flatten_expr e2)]
  | Quotient (e1, e2) ->
      [Quotient (flatten_expr e1, flatten_expr e2)]
  | Mod (e1, e2) ->
      [Mod (flatten_expr e1, flatten_expr e2)]
  | Shift_Right (e1, e2) ->
      [Shift_Right (flatten_expr e1, flatten_expr e2)]
  | Shift_Left (e1, e2) ->
      [Shift_Left (flatten_expr e1, flatten_expr e2)]

  | Packet_Access (s, e, nb_opt) ->
      [Packet_Access (s, flatten_expr e, nb_opt)]

and flatten_expr (e : expr) : expr =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> e

  | Plus es ->
      Plus
       (List.map flatten_expr_plus es
        |> List.concat)
  | Times es ->
      Times
       (List.map flatten_expr_times es
        |> List.concat)
  | Binary_And es ->
      Binary_And
       (List.map flatten_expr_band es
        |> List.concat)
  | Binary_Or es ->
      Binary_Or
       (List.map flatten_expr_bor es
        |> List.concat)
  | Binary_Xor es ->
      Binary_Xor
       (List.map flatten_expr_bor es
        |> List.concat)

  | Minus (e1, e2) ->
      Minus (flatten_expr e1, flatten_expr e2)
  | Quotient (e1, e2) ->
      Quotient (flatten_expr e1, flatten_expr e2)
  | Mod (e1, e2) ->
      Mod (flatten_expr e1, flatten_expr e2)
  | Shift_Right (e1, e2) ->
      Shift_Right (flatten_expr e1, flatten_expr e2)
  | Shift_Left (e1, e2) ->
      Shift_Left (flatten_expr e1, flatten_expr e2)

  | Packet_Access (s, e, nb_opt) ->
      Packet_Access (s, flatten_expr e, nb_opt)

let flatten_rel_expr (re : rel_expr) : rel_expr =
  match re with
  | Gt (e1, e2) ->
      Gt (flatten_expr e1, flatten_expr e2)
  | Lt (e1, e2) ->
      Lt (flatten_expr e1, flatten_expr e2)
  | Geq (e1, e2) ->
      Geq (flatten_expr e1, flatten_expr e2)
  | Leq (e1, e2) ->
      Leq (flatten_expr e1, flatten_expr e2)
  | Eq (e1, e2) ->
      Eq (flatten_expr e1, flatten_expr e2)
  | Neq (e1, e2) ->
      Neq (flatten_expr e1, flatten_expr e2)

let rec flatten_pcap_expression_conj (e : pcap_expression) : pcap_expression list =
  match e with
  | Primitive _
  | True
  | False -> [e]
  | Atom re -> [Atom (flatten_rel_expr re)]
  | Not e' -> [Not (flatten_pcap_expression e')]
  | And pes ->
      List.map flatten_pcap_expression_conj pes
      |> List.concat
  | Or pes ->
      [Or (List.map flatten_pcap_expression pes)]


and flatten_pcap_expression_disj (e : pcap_expression) : pcap_expression list =
  match e with
  | Primitive _
  | True
  | False -> [e]
  | Atom re -> [Atom (flatten_rel_expr re)]
  | Not e' -> [Not (flatten_pcap_expression e')]
  | And pes ->
      [And (List.map flatten_pcap_expression pes)]
  | Or pes ->
      List.map flatten_pcap_expression_disj pes
      |> List.concat

and flatten_pcap_expression (e : pcap_expression) : pcap_expression =
  match e with
  | Primitive _
  | True
  | False -> e
  | Atom re -> Atom (flatten_rel_expr re)
  | Not e' -> Not (flatten_pcap_expression e')
  | And pes ->
      And (List.map flatten_pcap_expression_conj pes
           |> List.concat)
  | Or pes ->
      Or (List.map flatten_pcap_expression_disj pes
          |> List.concat)

(*Indicates whether an expression includes "side-effecting" expressions, i.e.,
  references to offsets in the packet*)
let rec is_pure_expr (e : expr) : bool =
  match e with
  | Packet_Access _
  | Len ->
      (*This form of expressions make reference to the current packet, so they
        break the congruence of expressions wrt other packets*)
      false
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _ -> true
  | Plus es
  | Times es
  | Binary_And es
  | Binary_Or es
  | Binary_Xor es -> List.fold_right (fun e acc ->
      acc && is_pure_expr e) es true
  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Mod (e1, e2)
  | Shift_Right (e1, e2)
  | Shift_Left (e1, e2) -> is_pure_expr e1 && is_pure_expr e2

let is_pure_expr_re (re : rel_expr) : bool =
  match re with
  | Gt (e1, e2)
  | Lt (e1, e2)
  | Geq (e1, e2)
  | Leq (e1, e2)
  | Eq (e1, e2)
  | Neq (e1, e2) ->
      is_pure_expr e1 && is_pure_expr e2
(*NOTE we could argue that a pcap_expression containing a primitive
       predicate isn't pure, but in this case we're only judging the purity
       of expressions with a pcap_expressions*)
let rec is_pure_expr_pe (pe : pcap_expression) : bool =
  match pe with
  | Primitive _
  | True
  | False -> true
  | Atom re -> is_pure_expr_re re
  | Not pe -> is_pure_expr_pe pe
  | And pes
  | Or pes ->
      List.fold_right (fun pe acc ->
        acc && is_pure_expr_pe pe) pes true

(*FIXME move somewhere generic*)
let rec fold_right_lookahead_with_skip (f : 'a -> 'a list(*lookahead*) -> 'b(*acc*) -> ('b * bool(*skip*))) (l : 'a list) (acc : 'b) : 'b =
  match l with
  | x :: ((y :: rest) as remainder) ->
      let acc', skip = f x remainder acc in
      let remainder' = if skip then rest else remainder in
      fold_right_lookahead_with_skip f remainder' acc'
  | [x] -> fst (f x [] acc)
  | [] -> acc

let expand_ipv6 (ipv6_expr : string) : string =
  fold_right_lookahead_with_skip (fun hex_string(*This is a hex string, otherwise it wouldn't have parsed*) lookahead (encountered_double_colon, encountered_chars_so_far, acc) ->
    if hex_string = "" then
      if encountered_double_colon then
          failwith (Aux.error_output_prefix ^ ": ipv6_expand: " ^ "Malformed IPv6 address: too many colons.")
      else if not encountered_chars_so_far && lookahead <> [] && List.hd lookahead = "" then
        ((true, encountered_chars_so_far, "" :: acc), true)
      else if encountered_chars_so_far && lookahead = [""] then
        ((true, encountered_chars_so_far, "" :: acc), true)
      else ((true, encountered_chars_so_far, "" :: acc), false)
    else
      let expanded_string_prefix =
        if String.length hex_string = 1 then "000"
        else if String.length hex_string = 2 then "00"
        else if String.length hex_string = 3 then "0"
        else "" in
      begin
        ((encountered_double_colon, true, (expanded_string_prefix ^ hex_string) :: acc), false)
      end
   )
   (String.split_on_char ':' ipv6_expr) (false, false, [])
  |> (fun (_, _, split_ipv6) -> split_ipv6)
  |> List.rev
  |> (fun split_ipv6 ->
      List.fold_right (fun ipv6_part acc ->
        if ipv6_part <> "" then ipv6_part :: acc
        else
          let non_abstracted_parts = List.length split_ipv6 - 1 in
          let expected_ipv6_parts = 8 in
          let abstracted_parts = expected_ipv6_parts - non_abstracted_parts in
          let rec add_unabstracted_parts n acc =
            if n = 0 then acc
            else if n < 0 then
              failwith (Aux.error_output_prefix ^ ": ipv6_expand: " ^ "Malformed IPv6 address: too long.")
            else add_unabstracted_parts (n - 1) ("0000" :: acc) in
          add_unabstracted_parts abstracted_parts acc
      ) split_ipv6 [])
  |> String.concat ":"

let ipv6_expand (va : value_atom) : value_atom =
  match va with
  | IPv6_Address (orig_address, _) -> IPv6_Address (orig_address, expand_ipv6 orig_address)
  | IPv6_Network (prefix, _, prefix_len) ->
      let plen = int_of_string prefix_len in
      let max_ipv6_bits = 128 in
      if plen > max_ipv6_bits then
        failwith (Aux.error_output_prefix ^ ": ipv6_expand: " ^ "Invalid prefix length: " ^ prefix_len)
      else
        begin
        if plen = 0 then
          Aux.error_output ("WARN: prefix length (" ^ prefix_len ^ ") does not mask any bits.\n")
        else if plen = 128 then
          Aux.error_output ("WARN: prefix length (" ^ prefix_len ^ ") masks all bits -- this is indicating a host, not a network.\n")
        else ();
        IPv6_Network (prefix, expand_ipv6 prefix, prefix_len)
        end
  | _ -> failwith (Aux.error_output_prefix ^ ": ipv6_expand: " ^ "Not an IPv6 address/network: " ^ string_of_id_atom va)

let rec pe_weight (pe : pcap_expression) : int =
  match pe with
  | False
  | True -> 1
  | Primitive _
  | Atom _ -> 2
  | Not pe' -> 1 + pe_weight pe'
  | And pes
  | Or pes -> List.fold_right (fun pe' acc -> acc + pe_weight pe') pes 1


let rec contains_protocol_e (proto : proto) (e : expr) : bool =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> false

  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Mod (e1, e2)
  | Shift_Right (e1, e2)
  | Shift_Left (e1, e2) -> contains_protocol_e proto e1 || contains_protocol_e proto e2

  | Plus es
  | Times es
  | Binary_And es
  | Binary_Or es
  | Binary_Xor es -> List.fold_right (fun e' acc -> acc || contains_protocol_e proto e') es false

  | Packet_Access (proto_s, e', _) ->
      proto_s = string_of_proto proto || contains_protocol_e proto e'

let rec contains_protocol (proto : proto) (pe : pcap_expression) : bool =
  match pe with
  | False
  | True -> false
  | Primitive ((Some proto', _, _), _) when proto = proto' -> true
  | Primitive _ -> false
  | Atom re ->
      begin
        match re with
        | Gt (e1, e2)
        | Lt (e1, e2)
        | Geq (e1, e2)
        | Leq (e1, e2)
        | Eq (e1, e2)
        | Neq (e1, e2) -> contains_protocol_e proto e1 || contains_protocol_e proto e2
      end
  | Not pe' -> contains_protocol proto pe'
  | And pes
  | Or pes -> List.fold_right (fun pe' acc -> acc || contains_protocol proto pe') pes false

(*FIXME duplicates Names.protos_in_pe? *)
let rec list_protocols_e (protos : proto list) (e : expr) : proto list =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> protos

  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Mod (e1, e2)
  | Shift_Right (e1, e2)
  | Shift_Left (e1, e2) -> list_protocols_e (list_protocols_e protos e1) e2

  | Plus es
  | Times es
  | Binary_And es
  | Binary_Or es
  | Binary_Xor es -> List.fold_right (fun e' acc ->
     list_protocols_e acc e') es protos

  | Packet_Access (proto_s, e', _) ->
      list_protocols_e (proto_of_string proto_s :: protos) e'

let rec list_protocols (protos : proto list) (pe : pcap_expression) : proto list =
  match pe with
  | False
  | True -> protos
  | Primitive ((Some proto, _, _), Escaped_String proto_s) -> proto :: proto_of_string proto_s :: protos
  | Primitive ((Some proto, _, _), _) -> proto :: protos
  | Primitive _ -> protos
  | Atom re ->
      begin
        match re with
        | Gt (e1, e2)
        | Lt (e1, e2)
        | Geq (e1, e2)
        | Leq (e1, e2)
        | Eq (e1, e2)
        | Neq (e1, e2) -> list_protocols_e (list_protocols_e protos e1) e2
      end
  | Not pe' -> list_protocols protos pe'
  | And pes
  | Or pes -> List.fold_right (fun pe' acc ->
     list_protocols acc pe') pes protos
