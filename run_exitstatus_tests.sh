#!/bin/bash
# Caper: a pcap expression analysis utility.
# Run regression tests wrt exit status. This is applied to the same regressions suite for parsing.
# Nik Sultana, April 2023

# FIXME sensitive to the directory in which this is run
tests/syntax_regression.sh "./caper.byte -q -r -n -HTML -p -1stdin" tests/exitstatus_test.sh
